require_relative '../support/acceptance_tests_helper'

resource 'Acts' do
  header 'Content-Type', 'application/json'
  header 'access-token', :access_token_header
  header 'client', :client_header
  header 'uid', :uid_header
  
  let(:user)     { create(:admin_role) }
  let(:act)      { create :act }

  before { create_list :act, 3, :with_images, :with_additions, user: user }
  
  route 'api/v1/acts', 'Acts' do
    post 'Create' do

      example "Ok" do
        request = { act: create(:act, nact: '', nexp: '').attributes.except('id', 'created_at', 'updated_at').symbolize_keys }

        do_request(request)
        #byebug
        expect(status).to eq 200
      end

      example "Bad. Without specialist" do
        request = create(:act, nact: '', nexp: '').attributes.except('id', 'specialist_id', 'created_at', 'updated_at').symbolize_keys

        do_request(request)

        expect(status).to eq 422
      end

    end 

    get 'Get' do
      example "Ok" do
        
        do_request
        expect(status).to eq 200
      end
    end
  end
  
  route 'api/v1/acts/:id', 'Update Act' do
    let(:id)      { act.id }
        
    put 'Update' do
      example 'Ok' do
        request = { act: { ts_mark: 'new ts_mark' } }
        do_request(request)
        #byebug

        expect(status).to eq 200
      end
    end
  end
end