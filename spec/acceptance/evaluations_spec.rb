require_relative '../support/acceptance_tests_helper'

resource 'Evaluations' do
  header 'Content-Type', 'application/json'
  header 'access-token', :access_token_header
  header 'client', :client_header
  header 'uid', :uid_header
  
  let(:user)          { create(:admin_role) }
  let(:evaluation)    { create :evaluation }

  before { create_list :evaluation, 3, :with_images, :with_additions, user: user }
  
  route 'api/v1/evaluations', 'Evaluations' do
    post 'Create' do
      
      example "Ok" do
        request = { evaluation: create(:evaluation).attributes.except('id', 'created_at', 'updated_at').symbolize_keys }
        
        do_request(request)

        expect(status).to eq 200
      end

      example "Bad. Without object" do
        request = create(:evaluation).attributes.except('id', 'object', 'created_at', 'updated_at').symbolize_keys

        do_request(request)

        expect(status).to eq 422
      end
    end 

    get 'Get' do
      example "Ok" do
        
        do_request
        expect(status).to eq 200
      end
    end
  end
  
  route 'api/v1/evaluations/:id', 'Update Evaluation' do
    let(:id)      { evaluation.id }

    put 'Update' do
      example 'Ok' do
        request = { evaluation: { owner: 'new owner' } }
        do_request(request)
        #byebug

        expect(status).to eq 200
      end
    end
  end
end