describe ActPolicy do
  subject { described_class.new(user, act) }

  let(:act)  { create(:act) }
  
  context 'being a visitor' do
    let(:user) { nil }

    it { is_expected.to forbid_actions([:index, :show, :edit, :create, :destroy, :clodup, :download_xls]) }
  end

  context 'being an administrator' do
    let(:user) { create(:admin_role) }

    it { is_expected.to permit_actions([:index, :show, :edit, :create, :destroy, :clodup, :download_xls]) }
  end

  context 'editing my Act' do
    let(:user) { create(:expert_role) }
    let(:act)  { create(:act, expert: user, done: false) }

    it { is_expected.to permit_actions([:show, :edit, :clodup]) }
  end

  context 'editing closed Act' do
    let(:user) { create(:expert_role) }
    let(:act)  { create(:act, expert: user, done: true) }

    it { is_expected.to forbid_actions([:edit]) }
  end

  context 'editing other Act' do
    let(:user) { create(:expert_role) }
    let(:act)  { create(:act, done: false) }
    
    it { is_expected.to forbid_action(:edit) }
  end

end