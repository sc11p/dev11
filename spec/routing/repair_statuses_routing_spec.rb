require "rails_helper"

RSpec.describe RepairStatusesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/repair_statuses").to route_to("repair_statuses#index")
    end

    it "routes to #new" do
      expect(get: "/repair_statuses/new").to route_to("repair_statuses#new")
    end

    it "routes to #show" do
      expect(get: "/repair_statuses/1").to route_to("repair_statuses#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/repair_statuses/1/edit").to route_to("repair_statuses#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/repair_statuses").to route_to("repair_statuses#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/repair_statuses/1").to route_to("repair_statuses#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/repair_statuses/1").to route_to("repair_statuses#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/repair_statuses/1").to route_to("repair_statuses#destroy", id: "1")
    end
  end
end
