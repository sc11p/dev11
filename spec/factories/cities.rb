# == Schema Information
#
# Table name: cities
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

FactoryBot.define do
  factory :city do
    name       { Faker::Address.city }
  end
end
