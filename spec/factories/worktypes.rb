# == Schema Information
#
# Table name: worktypes
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  price      :bigint
#  created_at :datetime
#  updated_at :datetime
#

FactoryBot.define do
  factory :worktype do
    #name       { Faker::Types.rb_string }
    #sequence(:id)
    
    name do [
      'Экспертиза ТС', 'Акт осмотра', 'Трасология', 'УТС', 'Предстраховой осмотр',
      'Перерасчет', 'Рыночная стоимость', 'Участие в суде', 'Расчет износа',
      'Экспресс экспертиза', 'Диагностика SRS', 'Осмотр места ДТП', 'Справка о стоимости',
      'Годные остатки', 'Качество ремонта', 'Соглашение'].sample
    end
    price      { Faker::Number.number(digits: 3) }
  end
end
