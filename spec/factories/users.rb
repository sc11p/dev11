# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :bigint           default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  name                   :string(255)
#  role                   :bigint
#  active                 :boolean          default(TRUE), not null
#  settings               :jsonb            not null
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  tokens                 :text
#  allow_password_change  :boolean          default(TRUE), not null
#

FactoryBot.define do
  factory :user, aliases: [:expert, :specialist, :customer, :appraiser] do

    email      { Faker::Internet.unique.email }
    password   { Faker::Internet.password(min_length: 8) }
    name       { Faker::Name.unique.name }
    active     { true }
    uid        { Faker::Internet.uuid }
    settings   { { paging_per: 20 } }
    #roles      { [ Role.find_or_create_by(name: 'admin') ] }
    
    factory :admin_role do
      roles {
        [
          Role.find_or_create_by(name: 'admin')
        ]
      }
    end
    
    factory :expert_role do
      roles {
        [
          Role.find_or_create_by(name: 'expert'),
          Role.find_or_create_by(name: 'specialist')
        ]
      }
    end
    
  end
end
