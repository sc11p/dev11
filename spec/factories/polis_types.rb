# == Schema Information
#
# Table name: polis_types
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

FactoryBot.define do
  factory :polis_type do
    #name       { Faker::Types.rb_string }
    name do [
      'ОСАГО', 'КАСКО', 'Не установлен'].sample
    end
    
  end
end
