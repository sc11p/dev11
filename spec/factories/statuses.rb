# == Schema Information
#
# Table name: statuses
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

FactoryBot.define do
  factory :status do
    #name       { Faker::Types.rb_string }
    #sequence(:id)
    name do [
      'не распределено', 'в работе', 'готово', 'выезд не распределен',
      'распределено на выезд', 'назначено в сервис', 'согласование'].sample
    end
  end
end
