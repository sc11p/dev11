# == Schema Information
#
# Table name: acts
#
#  id             :bigint           not null, primary key
#  nact           :bigint
#  created_at     :datetime
#  updated_at     :datetime
#  nexp           :bigint
#  worktype_id    :bigint
#  dop            :boolean
#  city_id        :bigint
#  place          :string(255)
#  done           :boolean          default(FALSE), not null
#  ts_mark        :string(255)
#  ts_model       :string(255)
#  date           :date
#  datexp         :date
#  customer_id    :bigint
#  ncase          :string(255)
#  ts_number      :string(255)
#  vin            :string(255)
#  polis_type_id  :bigint
#  polis_number   :string(255)
#  owner          :string(255)
#  owner_phone    :string(255)
#  status         :string(255)
#  expert_id      :bigint
#  specialist_id  :bigint
#  user_id        :bigint           not null
#  status_id      :bigint
#  viezd          :bigint
#  km             :bigint
#  loss           :bigint
#  pvu            :boolean
#  complete_on    :date
#  summ_viezd     :bigint
#  summ_viezd_exp :bigint
#  summ_act       :bigint
#  summ_exp       :bigint
#  summ_prm       :bigint
#  nal            :boolean
#  prt            :bigint           default(50)
#  crime          :boolean
#  notpresent     :boolean
#  collect        :boolean
#  workshop_id    :integer
#  start_date     :date
#  start_time     :time
#  repair         :boolean          default(FALSE)
#  loss_full      :decimal(9, 2)
#  loss_wear      :decimal(9, 2)
#  loss_bill      :decimal(9, 2)
#
FactoryBot.define do
  
  factory :act do
    
    association :worktype, strategy: :create
    association :status, strategy: :create
    association :city, strategy: :create
    association :polis_type, strategy: :create
    #association :user, strategy: :create, name: "From ACT"
    # worktype
    # status
    # city
    # polis_type
    # user
    specialist
    expert
    user
    customer


    nexp            { Faker::Number.number(digits: 5) }
    nact            { Faker::Number.number(digits: 5) }
    
    #specialist      { association :user }
    date            { Faker::Date.in_date_period }
    complete_on     { Faker::Date.in_date_period }
    dop             { Faker::Boolean.boolean }
    #customer        { association :user }
    polis_number    { Faker::Number.number(digits: 10) }
    pvu             { Faker::Boolean.boolean }
    ncase           { Faker::Number.number(digits: 10) }
    place           { Faker::Address.street_name }
    viezd           { Faker::Boolean.boolean }
    km              { Faker::Number.digit }
    datexp          { Faker::Date.in_date_period  }
    loss            { Faker::Number.number(digits: 6) }
    done            { Faker::Boolean.boolean }
    #expert          { association :user }
    owner           { Faker::Name.unique.name }
    owner_phone     { Faker::PhoneNumber.cell_phone }
    ts_mark         { Faker::Vehicle.make }
    ts_model        { Faker::Vehicle.model }
    ts_number       { Faker::Vehicle.license_plate }
    vin             { Faker::Vehicle.vin }
    summ_act        { '500' }
    summ_viezd      { '1000' }
    summ_viezd_exp  { '1500' }
    summ_exp        { '5000' }
    summ_prm        { '100' }
    nal             { Faker::Boolean.boolean }
    prt             { Faker::Number.between(from: 1, to: 99) }
    crime           { Faker::Boolean.boolean }
    # start_time      { "1" }
    collect         { Faker::Boolean.boolean }
    notpresent      { Faker::Boolean.boolean }

    trait :with_images do
      after(:build) do |act|
        act.images.attach(
          io: File.open(Rails.root.join('spec', 'fixtures', 'files', 'blank.jpg')),
          filename: 'blank.jpg'
        )
      end
    end

    trait :with_additions do
      after(:build) do |act|
        act.additions.attach(
          io: File.open(Rails.root.join('spec', 'fixtures', 'files', 'blank.pdf')),
          filename: 'blank.pdf'
        )
      end
    end

  end
end
