# == Schema Information
#
# Table name: evaluations
#
#  id             :bigint           not null, primary key
#  nact           :bigint
#  date           :date
#  appraiser_id   :bigint
#  oldcomments    :string(255)
#  city_id        :bigint
#  place          :string(255)
#  otype_id       :bigint
#  status_id      :bigint
#  user_id        :bigint           not null
#  created_at     :datetime
#  updated_at     :datetime
#  customer_id    :bigint
#  specialist_id  :bigint
#  summ_act       :bigint
#  summ_viezd     :bigint
#  summ_viezd_exp :bigint
#  summ_exp       :bigint
#  nocenka        :bigint
#  nal            :boolean          default(FALSE)
#  viezd          :bigint
#  km             :bigint
#  datexp         :date
#  object         :string(255)
#  ncase          :string(255)
#  polis_number   :string(255)
#  owner          :string(255)
#  done           :boolean          default(FALSE), not null
#  polis_type_id  :bigint           default(3), not null
#  collect        :boolean          default(FALSE), not null
#  paymentcontrol :boolean          default(FALSE), not null
#
FactoryBot.define do
  
  factory :evaluation do
    
    association :otype, factory: :otype, strategy: :create
    association :status, strategy: :create
    association :city, strategy: :create
    association :polis_type, strategy: :create
    # association :user, strategy: :create
    # user
    # otype
    # polis_type
    # status
    # city
    specialist
    appraiser
    user
    customer

    nocenka         { Faker::Number.number(digits: 5) }
    nact            { Faker::Number.number(digits: 5) }
    #specialist      { association :user }
    date            { Faker::Date.in_date_period }
    #customer        { association :user }
    polis_number    { Faker::Number.number(digits: 10) }
    ncase           { Faker::Number.number(digits: 10) }
    place           { Faker::Address.street_name }
    viezd           { Faker::Boolean.boolean }
    km              { Faker::Number.digit }
    datexp          { Faker::Date.in_date_period  }
    done            { Faker::Boolean.boolean }
    #appraiser       { association :user }
    owner           { Faker::Name.unique.name }
    object          { Faker::Commerce.product_name }
    summ_act        { '500' }
    summ_viezd      { '1000' }
    summ_viezd_exp  { '1500' }
    summ_exp        { '5000' }
    nal             { Faker::Boolean.boolean }
    collect         { Faker::Boolean.boolean }
    paymentcontrol  { Faker::Boolean.boolean }

    trait :with_images do
      after(:build) do |evaluation|
        evaluation.images.attach(
          io: File.open(Rails.root.join('spec', 'fixtures', 'files', 'blank.jpg')),
          filename: 'blank.jpg'
        )
      end
    end

    trait :with_additions do
      after(:build) do |evaluation|
        evaluation.additions.attach(
          io: File.open(Rails.root.join('spec', 'fixtures', 'files', 'blank.pdf')),
          filename: 'blank.pdf'
        )
      end
    end

  end
end
