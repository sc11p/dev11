# == Schema Information
#
# Table name: otypes
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  price      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

FactoryBot.define do
  factory :otype do
    #name       { Faker::Types.rb_string }
    #sequence(:id)
    
    name do [
      'Смета', 'Оценка рыночной стоимости', 'Акт осмотра', 'Справка о стоимости'].sample
    end
    # price      { Faker::Number.number(digits: 3) }
  end
end
