namespace :roles do
  desc "Add roles"
  task add_roles: :environment do
    Role.create(name: 'admin')
    Role.create(name: 'expert')
    Role.create(name: 'specialist')
    Role.create(name: 'customer')
    Role.create(name: 'appraiser')
    Role.create(name: 'viewer')
  end
end

# enum role: { admin: 0, expert: 1, specialist: 2, customer: 3, ocenshik: 4, demo: 5, viezdmaster: 6, viewer: 7 }
