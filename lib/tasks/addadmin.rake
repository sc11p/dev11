namespace :users do
  desc "Add admin role"
  task add_admin: :environment do
    User.find(505).admin!
  end
end
