class AddCollectToEvaluations < ActiveRecord::Migration[6.1]
  def change
    add_column :evaluations, :collect, :boolean, null: false, default: false
  end
end
