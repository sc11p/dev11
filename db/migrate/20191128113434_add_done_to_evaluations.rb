class AddDoneToEvaluations < ActiveRecord::Migration[6.0]
  def change
    add_column :evaluations, :done, :boolean, null: false, default: false
  end
end
