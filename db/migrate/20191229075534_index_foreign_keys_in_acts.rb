class IndexForeignKeysInActs < ActiveRecord::Migration[6.0]
  def change
    add_index :acts, :polis_type_id
  end
end
