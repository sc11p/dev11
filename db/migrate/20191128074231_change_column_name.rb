class ChangeColumnName < ActiveRecord::Migration[6.0]
  def change
    # remove_index :ocenkas, :ocenshik_id
    rename_column :ocenkas, :ocenshik_id, :appraiser_id
    add_index :ocenkas, :appraiser_id
  end
end
