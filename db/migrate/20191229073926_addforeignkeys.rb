class Addforeignkeys < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :acts, :cities
    
    
    add_foreign_key :acts, :polis_types
    
    add_foreign_key :acts, :statuses
    # add_foreign_key :acts, :users
    add_foreign_key :acts, :worktypes

    # add_foreign_key :comments, :parents
    
    add_foreign_key :evaluations, :cities
    
    add_foreign_key :evaluations, :otypes
    add_foreign_key :evaluations, :polis_types
    
    add_foreign_key :evaluations, :statuses
    add_foreign_key :evaluations, :users
    
    add_foreign_key :prices, :users
    add_foreign_key :prices, :worktypes
  end
end
