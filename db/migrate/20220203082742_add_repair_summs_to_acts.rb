class AddRepairSummsToActs < ActiveRecord::Migration[6.1]
  def change
    add_column :acts, :loss_full, :decimal, precision: 9, scale: 2
    add_column :acts, :loss_wear, :decimal, precision: 9, scale: 2
    add_column :acts, :loss_bill, :decimal, precision: 9, scale: 2
  end
end
