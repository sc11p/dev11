class AddPolistypeToEvaluations < ActiveRecord::Migration[6.0]
  def change
    add_column :evaluations, :polis_type_id, :integer, default: 3
  end
end
