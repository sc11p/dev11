class AddRepairStatusIdToActs < ActiveRecord::Migration[6.1]
  def change
    add_column :acts, :repair_status_id, :integer
  end
end
