class AddPaymentcontrolToEvaluations < ActiveRecord::Migration[6.1]
  def change
    add_column :evaluations, :paymentcontrol, :boolean, null: false, default: false
  end
end
