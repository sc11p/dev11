class ChangeOcenkasToEvaluations < ActiveRecord::Migration[6.0]
  def change
    # add_index :ocenkas, :id
    # execute "ALTER INDEX ocenkas_pkey RENAME TO evaluations_pkey;"

    # execute "ALTER TABLE ocenkas DROP CONSTRAINT ocenkas_pkey;"
    # execute "ALTER TABLE ocenkas ADD PRIMARY KEY (id);"
    # remove_index :ocenkas, :city_id
    # remove_index :ocenkas, :customer_id
    # remove_index :ocenkas, :ocenshik_id
    # remove_index :ocenkas, :otype_id
    # remove_index :ocenkas, :status_id
    # remove_index :ocenkas, :user_id

    rename_table :ocenkas, :evaluations

    # add_index :evaluations, :city_id
    # add_index :evaluations, :customer_id
    # add_index :evaluations, :appraiser_id
    # add_index :evaluations, :otype_id
    # add_index :evaluations, :status_id
    # add_index :evaluations, :user_id
  end
end
