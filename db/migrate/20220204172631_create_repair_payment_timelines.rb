class CreateRepairPaymentTimelines < ActiveRecord::Migration[6.1]
  def change
    create_table :repair_payment_timelines do |t|
      t.integer :act_id
      t.integer :repair_payment_status_id

      t.timestamps
    end
  end
end
