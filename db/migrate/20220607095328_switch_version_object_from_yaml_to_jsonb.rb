class SwitchVersionObjectFromYamlToJsonb < ActiveRecord::Migration[7.0]
  def change
    # Create temporary columns to convert YAML to jsonb
    add_column :audits, :new_object, :jsonb
    add_column :audits, :new_object_changes, :jsonb

        
    reversible do |direction|
      direction.up do
        PaperTrail::Version.table_name='audits'
        PaperTrail::Version.where.not(object: nil).find_each do |audit|
          attributes = { new_object: YAML.load(audit.object, permitted_classes: [Date, Time, ActiveRecord::Type::Time::Value, BigDecimal]) }

          if audit.object_changes
            attributes[:new_object_changes] = YAML.load(audit.object_changes, permitted_classes: [Date, Time, ActiveRecord::Type::Time::Value, BigDecimal])
          end

          audit.update_columns(attributes)
        end
      end

      direction.down do
        PaperTrail::Version.where.not(new_object: nil).find_each do |audit|
          attributes = { object: YAML.dump(audit.new_object) }

          if audit.new_object_changes
            attributes[:object_changes] = YAML.dump(audit.new_object_changes)
          end

          audit.update_columns(attributes)
        end
      end
    end

    # Switch columns
    remove_column :audits, :object, :text
    remove_column :audits, :object_changes, :text
    rename_column :audits, :new_object, :object
    rename_column :audits, :new_object_changes, :object_changes
  end
end
