class ChangeStarttimeColumn < ActiveRecord::Migration[6.1]
  def change
    remove_column :acts, :start_time
    add_column :acts, :start_time, :time
    add_column :acts, :start_date, :date
  end
end
