class Workshoplands < ActiveRecord::Migration[6.1]
  def change
    create_table :workshoplands do |t|
      t.integer :city_id
      t.integer :workshop_id

      t.timestamps
    end
  end
end
