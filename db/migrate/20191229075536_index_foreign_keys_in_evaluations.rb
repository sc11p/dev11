class IndexForeignKeysInEvaluations < ActiveRecord::Migration[6.0]
  def change
    add_index :evaluations, :polis_type_id
    add_index :evaluations, :specialist_id
  end
end
