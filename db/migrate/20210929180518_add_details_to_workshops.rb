class AddDetailsToWorkshops < ActiveRecord::Migration[6.1]
  def change
    add_column :workshops, :email, :string
    add_column :workshops, :tel, :integer
    add_column :workshops, :address, :string
    add_column :workshops, :manager, :string
    add_column :workshops, :active, :boolean, null: false, default: true
  end
end
