class AddUsersToWorkshops < ActiveRecord::Migration[6.1]
  def change
    add_column :workshops, :user_id, :integer
  end
end
