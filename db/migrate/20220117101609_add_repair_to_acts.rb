class AddRepairToActs < ActiveRecord::Migration[6.1]
  def change
    add_column :acts, :repair, :boolean, null: false, default: false
  end
end
