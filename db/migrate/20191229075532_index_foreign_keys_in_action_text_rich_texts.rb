class IndexForeignKeysInActionTextRichTexts < ActiveRecord::Migration[6.0]
  def change
    add_index :action_text_rich_texts, :record_id
  end
end
