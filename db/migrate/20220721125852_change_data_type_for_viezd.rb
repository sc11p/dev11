class ChangeDataTypeForViezd < ActiveRecord::Migration[7.0]
  def change
    #change_column :acts, :viezd, 'boolean USING CAST(viezd AS boolean)'
    change_column :acts, :viezd,'boolean USING (CASE viezd WHEN \'1\' THEN \'t\'::boolean ELSE \'f\'::boolean END)'
  end
end
