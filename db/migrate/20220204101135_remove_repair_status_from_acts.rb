class RemoveRepairStatusFromActs < ActiveRecord::Migration[6.1]
  def change
    remove_column :acts, :repair_status_id
  end
end
