class AddWorkshopToActs < ActiveRecord::Migration[6.1]
  def change
    add_column :acts, :workshop_id, :integer
  end
end
