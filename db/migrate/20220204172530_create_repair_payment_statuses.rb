class CreateRepairPaymentStatuses < ActiveRecord::Migration[6.1]
  def change
    create_table :repair_payment_statuses do |t|
      t.string :name

      t.timestamps
    end
  end
end
