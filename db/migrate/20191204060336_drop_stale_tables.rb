class DropStaleTables < ActiveRecord::Migration[6.0]
  def change
    drop_table :comments
    drop_table :customers
    drop_table :dtypes
    drop_table :expertises
    drop_table :dtpcars
    drop_table :ocenshiks
    drop_table :ofiles
    drop_table :pictures
    drop_table :specialists
    drop_table :statuses2
    drop_table :users_roles
    drop_table :experts
  end
end
