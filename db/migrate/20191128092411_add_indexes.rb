class AddIndexes < ActiveRecord::Migration[6.0]
  def change
    def up
      add_index :evaluations, :city_id
      add_index :evaluations, :customer_id
      add_index :evaluations, :appraiser_id
      add_index :evaluations, :otype_id
      add_index :evaluations, :status_id
      add_index :evaluations, :user_id

      add_index :acts, :city_id
      add_index :acts, :customer_id
      add_index :acts, :expert_id
      add_index :acts, :worktype_id
      add_index :acts, :status_id
      add_index :acts, :user_id
    end

    def down
      remove_index :evaluations, :city_id
      remove_index :evaluations, :customer_id
      remove_index :evaluations, :appraiser_id
      remove_index :evaluations, :otype_id
      remove_index :evaluations, :status_id
      remove_index :evaluations, :user_id

      remove_index :acts, :city_id
      remove_index :acts, :customer_id
      remove_index :acts, :expert_id
      remove_index :acts, :worktype_id
      remove_index :acts, :status_id
      remove_index :acts, :user_id
    end
  end
end
