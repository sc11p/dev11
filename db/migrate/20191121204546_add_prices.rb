class AddPrices < ActiveRecord::Migration[6.0]
  def change
    create_table :prices do |t|
      t.belongs_to :user
      t.belongs_to :worktype
      t.integer :cost
      t.timestamps
    end
  end
end
