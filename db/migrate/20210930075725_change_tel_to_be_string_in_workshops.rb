class ChangeTelToBeStringInWorkshops < ActiveRecord::Migration[6.1]
  def change
    change_column :workshops, :tel, :string
  end
end
