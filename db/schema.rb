# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_07_21_125852) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_id"], name: "index_action_text_rich_texts_on_record_id"
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_id"], name: "index_active_storage_attachments_on_record_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "acts", force: :cascade do |t|
    t.bigint "nact"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.bigint "nexp"
    t.bigint "worktype_id"
    t.boolean "dop"
    t.bigint "city_id"
    t.string "place", limit: 255
    t.boolean "done", default: false, null: false
    t.string "ts_mark", limit: 255
    t.string "ts_model", limit: 255
    t.date "date"
    t.date "datexp"
    t.bigint "customer_id"
    t.string "ncase", limit: 255
    t.string "ts_number", limit: 255
    t.string "vin", limit: 255
    t.bigint "polis_type_id"
    t.string "polis_number", limit: 255
    t.string "owner", limit: 255
    t.string "owner_phone", limit: 255
    t.string "status", limit: 255
    t.bigint "expert_id"
    t.bigint "specialist_id"
    t.bigint "user_id", null: false
    t.bigint "status_id"
    t.boolean "viezd"
    t.bigint "km"
    t.bigint "loss"
    t.boolean "pvu"
    t.date "complete_on"
    t.bigint "summ_viezd"
    t.bigint "summ_viezd_exp"
    t.bigint "summ_act"
    t.bigint "summ_exp"
    t.bigint "summ_prm"
    t.boolean "nal"
    t.bigint "prt", default: 50
    t.boolean "crime"
    t.boolean "notpresent"
    t.boolean "collect"
    t.integer "workshop_id"
    t.time "start_time"
    t.date "start_date"
    t.boolean "repair", default: false
    t.decimal "loss_full", precision: 9, scale: 2
    t.decimal "loss_wear", precision: 9, scale: 2
    t.decimal "loss_bill", precision: 9, scale: 2
    t.index ["city_id"], name: "index_acts_on_city_id"
    t.index ["customer_id"], name: "index_acts_on_customer_id"
    t.index ["expert_id"], name: "index_acts_on_expert_id"
    t.index ["polis_type_id"], name: "index_acts_on_polis_type_id"
    t.index ["specialist_id"], name: "index_acts_on_specialist_id"
    t.index ["status_id"], name: "index_acts_on_status_id"
    t.index ["user_id"], name: "index_acts_on_user_id"
    t.index ["worktype_id"], name: "index_acts_on_worktype_id"
  end

  create_table "assignments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_assignments_on_role_id"
    t.index ["user_id"], name: "index_assignments_on_user_id"
  end

  create_table "audits", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.datetime "created_at", precision: nil
    t.jsonb "object"
    t.jsonb "object_changes"
    t.index ["item_type", "item_id"], name: "index_audits_on_item_type_and_item_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name", limit: 255
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "user_id"
    t.string "commentable_type"
    t.bigint "commentable_id"
    t.integer "parent_id"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
    t.index ["parent_id"], name: "index_comments_on_parent_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "discussions", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "evaluations", force: :cascade do |t|
    t.bigint "nact"
    t.date "date"
    t.bigint "appraiser_id"
    t.string "oldcomments", limit: 255
    t.bigint "city_id"
    t.string "place", limit: 255
    t.bigint "otype_id"
    t.bigint "status_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.bigint "customer_id"
    t.bigint "specialist_id"
    t.bigint "summ_act"
    t.bigint "summ_viezd"
    t.bigint "summ_viezd_exp"
    t.bigint "summ_exp"
    t.bigint "nocenka"
    t.boolean "nal", default: false
    t.bigint "viezd"
    t.bigint "km"
    t.date "datexp"
    t.string "object", limit: 255
    t.string "ncase", limit: 255
    t.string "polis_number", limit: 255
    t.string "owner", limit: 255
    t.boolean "done", default: false, null: false
    t.bigint "polis_type_id", default: 3, null: false
    t.boolean "collect", default: false, null: false
    t.boolean "paymentcontrol", default: false, null: false
    t.index ["appraiser_id"], name: "index_evaluations_on_appraiser_id"
    t.index ["city_id"], name: "index_evaluations_on_city_id"
    t.index ["customer_id"], name: "index_evaluations_on_customer_id"
    t.index ["otype_id"], name: "index_evaluations_on_otype_id"
    t.index ["polis_type_id"], name: "index_evaluations_on_polis_type_id"
    t.index ["specialist_id"], name: "index_evaluations_on_specialist_id"
    t.index ["status_id"], name: "index_evaluations_on_status_id"
    t.index ["user_id"], name: "index_evaluations_on_user_id"
  end

  create_table "otypes", force: :cascade do |t|
    t.string "name", limit: 255
    t.string "price", limit: 255
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "polis_types", force: :cascade do |t|
    t.string "name", limit: 255
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "prices", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "worktype_id"
    t.integer "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_prices_on_user_id"
    t.index ["worktype_id"], name: "index_prices_on_worktype_id"
  end

  create_table "repair_payment_statuses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "repair_payment_timelines", force: :cascade do |t|
    t.integer "act_id"
    t.integer "repair_payment_status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "repair_statuses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "repair_timelines", force: :cascade do |t|
    t.integer "act_id"
    t.integer "repair_status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name", limit: 255
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "users", force: :cascade do |t|
    t.string "email", limit: 255, default: "", null: false
    t.string "encrypted_password", limit: 255, default: "", null: false
    t.string "reset_password_token", limit: 255
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.bigint "sign_in_count", default: 0
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip", limit: 255
    t.string "last_sign_in_ip", limit: 255
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "name", limit: 255
    t.bigint "role"
    t.boolean "active", default: true, null: false
    t.jsonb "settings", default: {}, null: false
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.text "tokens"
    t.boolean "allow_password_change", default: true, null: false
    t.text "private_api_key"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["settings"], name: "index_users_on_settings", using: :gin
  end

  create_table "workshoplands", force: :cascade do |t|
    t.integer "city_id"
    t.integer "workshop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "workshops", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.string "email"
    t.string "tel"
    t.string "address"
    t.string "manager"
    t.boolean "active", default: true, null: false
  end

  create_table "worktypes", force: :cascade do |t|
    t.string "name", limit: 255
    t.bigint "price"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "acts", "cities"
  add_foreign_key "acts", "polis_types"
  add_foreign_key "acts", "statuses"
  add_foreign_key "acts", "worktypes"
  add_foreign_key "assignments", "roles"
  add_foreign_key "assignments", "users"
  add_foreign_key "comments", "users"
  add_foreign_key "evaluations", "cities"
  add_foreign_key "evaluations", "otypes"
  add_foreign_key "evaluations", "polis_types"
  add_foreign_key "evaluations", "statuses"
  add_foreign_key "evaluations", "users"
  add_foreign_key "prices", "users"
  add_foreign_key "prices", "worktypes"
end
