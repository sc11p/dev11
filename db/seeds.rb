# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Create roles..."
  Role.create(name: 'admin')
  Role.create(name: 'expert')
  Role.create(name: 'specialist')
  Role.create(name: 'customer')
  Role.create(name: 'appraiser')
  Role.create(name: 'viewer')

  ids = Role.all.ids

puts "Create admin user..."
User.create do |user|
  user.email    = 'evt@rocenka.ru'
  user.password = 'astalavista'
  user.active = true
  user.role_ids = ids
  if user.try(:admin?)
    puts 'Admin'
  end
end

#Post.reindex

#puts "Add articles about programming languages from Wikipedia..."
#WikipediaJob.perform_now