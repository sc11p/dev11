# frozen_string_literal: true

class ActUpdateChannel < ApplicationCable::Channel
  def subscribed
    stop_all_streams

    return unless params[:act_id]

    act = Act.find(params[:act_id])
    stream_for act
  end

  def unsubscribed
    stop_all_streams
  end
  end
