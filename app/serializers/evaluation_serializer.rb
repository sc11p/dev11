# == Schema Information
#
# Table name: evaluations
#
#  id             :bigint           not null, primary key
#  nact           :bigint
#  date           :date
#  appraiser_id   :bigint
#  oldcomments    :string(255)
#  city_id        :bigint
#  place          :string(255)
#  otype_id       :bigint
#  status_id      :bigint
#  user_id        :bigint           not null
#  created_at     :datetime
#  updated_at     :datetime
#  customer_id    :bigint
#  specialist_id  :bigint
#  summ_act       :bigint
#  summ_viezd     :bigint
#  summ_viezd_exp :bigint
#  summ_exp       :bigint
#  nocenka        :bigint
#  nal            :boolean          default(FALSE)
#  viezd          :bigint
#  km             :bigint
#  datexp         :date
#  object         :string(255)
#  ncase          :string(255)
#  polis_number   :string(255)
#  owner          :string(255)
#  done           :boolean          default(FALSE), not null
#  polis_type_id  :bigint           default(3), not null
#  collect        :boolean          default(FALSE), not null
#  paymentcontrol :boolean          default(FALSE), not null
#
class EvaluationSerializer
  include JSONAPI::Serializer

  attributes :id, :nact, :date, :appraiser_id, :oldcomments, :city_id, :place, :otype_id, :status_id, :user_id, :created_at, :updated_at, :customer_id, :specialist_id, :summ_act, :summ_viezd, :summ_viezd_exp, :summ_exp, :nocenka, :nal, :viezd, :km, :datexp, :object, :ncase, :polis_number, :owner, :done, :polis_type_id, :collect, :paymentcontrol, :image_urls, :addition_urls

  attribute :additions do |object|
    if object.additions.attached?
      urls = object.additions.map { |x| { url: rails_blob_url(x), id: x.id } }
    end
  end

  attribute :images do |object|
    if object.images.attached?
      urls = object.images.map { |x| { url: rails_blob_url(x), id: x.id } }
    end
  end
  
end
