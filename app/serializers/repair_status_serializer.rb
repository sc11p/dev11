class RepairStatusSerializer
  include JSONAPI::Serializer
  attribute :name, :created_at
end