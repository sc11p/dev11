# == Schema Information
#
# Table name: acts
#
#  id             :bigint           not null, primary key
#  nact           :bigint
#  created_at     :datetime
#  updated_at     :datetime
#  nexp           :bigint
#  worktype_id    :bigint
#  dop            :boolean
#  city_id        :bigint
#  place          :string(255)
#  done           :boolean          default(FALSE), not null
#  ts_mark        :string(255)
#  ts_model       :string(255)
#  date           :date
#  start_time     :datetime
#  datexp         :date
#  customer_id    :bigint
#  ncase          :string(255)
#  ts_number      :string(255)
#  vin            :string(255)
#  polis_type_id  :bigint
#  polis_number   :string(255)
#  owner          :string(255)
#  owner_phone    :string(255)
#  status         :string(255)
#  expert_id      :bigint
#  specialist_id  :bigint
#  user_id        :bigint           not null
#  status_id      :bigint
#  viezd          :bigint
#  km             :bigint
#  loss           :bigint
#  pvu            :boolean
#  complete_on    :date
#  summ_viezd     :bigint
#  summ_viezd_exp :bigint
#  summ_act       :bigint
#  summ_exp       :bigint
#  summ_prm       :bigint
#  nal            :boolean
#  prt            :bigint           default(50)
#  crime          :boolean
#  notpresent     :boolean
#  collect        :boolean
#
class ActPreviewSerializer
  include JSONAPI::Serializer
  singleton_class.include Rails.application.routes.url_helpers

  attributes :id, :worktype, :nexp, :nact, :date, :specialist, :expert, :customer, :status, :polis_type, :ts_mark, :ts_model, :ts_number, :city, :place,:workshop, :start_time, :start_date
  # :image_urls, :addition_urls
    
  # attribute :images do |object|
  #   if object.images.attached?
  #     urls = object.images.map { |x| { url: rails_blob_url(x), id: x.id } }
  #   end
  # end

  # attribute :additions do |object|
  #   if object.additions.attached?
  #     urls = object.additions.map { |x| { url: rails_blob_url(x), id: x.id } }
  #   end
  # end
  
  # has_many :comments, as: :commentable

  # belongs_to :worktype
  # belongs_to :polis_type
  # belongs_to :user
  # belongs_to :customer, class_name: 'User', foreign_key: 'customer_id', optional: true
  # belongs_to :expert, class_name: 'User', foreign_key: 'expert_id', optional: true
  # belongs_to :specialist, class_name: 'User', foreign_key: 'specialist_id', optional: true
  # belongs_to :status
  # belongs_to :city
  
end
