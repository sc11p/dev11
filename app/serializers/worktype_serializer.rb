# == Schema Information
#
# Table name: worktypes
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  price      :bigint
#  created_at :datetime
#  updated_at :datetime
#
class WorktypeSerializer
  include JSONAPI::Serializer
  attributes :name
end
