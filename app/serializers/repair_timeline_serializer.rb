class RepairTimelineSerializer
    include JSONAPI::Serializer
    attribute :repair_status, :act_id, :created_at
end
