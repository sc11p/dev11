class RepairPaymentTimelineSerializer
    include JSONAPI::Serializer
    attribute :repair_payment_status, :act_id, :created_at
end

