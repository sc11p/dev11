# frozen_string_literal: true

# == Schema Information
#
# Table name: cities
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#
class City < ApplicationRecord
  has_many :acts
  has_many :evaluations
  has_many :workshoplands
  has_many :workshops, through: :workshoplands
end
