# frozen_string_literal: true

# == Schema Information
#
# Table name: worktypes
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  price      :bigint
#  created_at :datetime
#  updated_at :datetime
#
class Worktype < ApplicationRecord
  has_many :acts
  has_many :prices
  has_many :users, through: :prices

  def self.ransackable_attributes(auth_object = nil)
    ["created_at", "id", "name", "price", "updated_at"]
  end
  
end
