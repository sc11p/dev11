# == Schema Information
#
# Table name: repair_payment_statuses
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class RepairPaymentStatus < ApplicationRecord
  has_many :repair_payment_timelines
  has_many :acts, through: :repair_payment_timelines
  
  def self.ransackable_attributes(auth_object = nil)
    ["created_at", "id", "name", "updated_at"]
  end
  
end
