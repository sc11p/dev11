# == Schema Information
#
# Table name: repair_payment_timelines
#
#  id                       :bigint           not null, primary key
#  act_id                   :integer
#  repair_payment_status_id :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
class RepairPaymentTimeline < ApplicationRecord
  belongs_to :act
  belongs_to :repair_payment_status
end
