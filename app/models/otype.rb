# frozen_string_literal: true

# == Schema Information
#
# Table name: otypes
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  price      :string(255)
#  created_at :datetime
#  updated_at :datetime
#
class Otype < ApplicationRecord
  has_many :evaluations

  def self.ransackable_attributes(auth_object = nil)
    ["created_at", "id", "name", "price", "updated_at"]
  end
  
end
