# frozen_string_literal: true

# == Schema Information
#
# Table name: acts
#
#  id             :bigint           not null, primary key
#  nact           :bigint
#  created_at     :datetime
#  updated_at     :datetime
#  nexp           :bigint
#  worktype_id    :bigint
#  dop            :boolean
#  city_id        :bigint
#  place          :string(255)
#  done           :boolean          default(FALSE), not null
#  ts_mark        :string(255)
#  ts_model       :string(255)
#  date           :date
#  datexp         :date
#  customer_id    :bigint
#  ncase          :string(255)
#  ts_number      :string(255)
#  vin            :string(255)
#  polis_type_id  :bigint
#  polis_number   :string(255)
#  owner          :string(255)
#  owner_phone    :string(255)
#  status         :string(255)
#  expert_id      :bigint
#  specialist_id  :bigint
#  user_id        :bigint           not null
#  status_id      :bigint
#  viezd          :bigint
#  km             :bigint
#  loss           :bigint
#  pvu            :boolean
#  complete_on    :date
#  summ_viezd     :bigint
#  summ_viezd_exp :bigint
#  summ_act       :bigint
#  summ_exp       :bigint
#  summ_prm       :bigint
#  nal            :boolean
#  prt            :bigint           default(50)
#  crime          :boolean
#  notpresent     :boolean
#  collect        :boolean
#  workshop_id    :integer
#  start_date     :date
#  start_time     :time
#  repair         :boolean          default(FALSE)
#  loss_full      :decimal(9, 2)
#  loss_wear      :decimal(9, 2)
#  loss_bill      :decimal(9, 2)
#
class Act < ApplicationRecord
  has_many :comments, as: :commentable

  has_many_attached :images
  has_many_attached :calculations
  has_many_attached :additions

  has_many :repair_timelines
  has_many :repair_statuses, through: :repair_timelines
  accepts_nested_attributes_for :repair_statuses

  has_many :repair_payment_timelines
  has_many :repair_payment_statuses, through: :repair_payment_timelines
  accepts_nested_attributes_for :repair_payment_statuses

  has_rich_text :content

  belongs_to :worktype
  belongs_to :polis_type
  belongs_to :user
  belongs_to :workshop, optional: true
  belongs_to :customer, class_name: 'User', foreign_key: 'customer_id', optional: true
  belongs_to :expert, class_name: 'User', foreign_key: 'expert_id', optional: true
  belongs_to :specialist, class_name: 'User', foreign_key: 'specialist_id', optional: true
  belongs_to :status
  belongs_to :city

  has_paper_trail versions: { name: :audits, class_name: 'Audit' }, skip: [:created_at, :updated_at]

  validates :date, :worktype_id, :customer_id, :specialist_id, :city_id, :status_id, :polis_type_id, :user_id, presence: true
  validates :summ_viezd, :summ_act, numericality: { greater_than_or_equal_to: 50, less_than: 500_000 }, allow_nil: true
  validates :summ_exp, numericality: { greater_than_or_equal_to: 50, less_than: 500_000 }, allow_nil: true
  validates :nexp, uniqueness: { scope: :expert_id, conditions: -> { where('created_at > ?', 1.month.ago) }, if: -> { nexp.present? } }
  validates :nact, uniqueness: { scope: :specialist_id, conditions: -> { where('created_at >= ?', Date.today.at_beginning_of_month) }, if: -> { nact.present? } }
  validates :expert_id, presence: true, if: :in_work?

  scope :done, ->(_boolean = true) { where(done: true) }
  scope :inthework, -> { where(status_id: '2') }
  scope :unregistered, -> { where(status_id: '1') }
  scope :priority, -> { where('prt > ?', 50) }
  scope :stale, -> { where('Time.zone.now.to_date - act.date.to_date < ?', 2) }
  scope :by_year, ->(year) { where('extract(year from created_at) + 0 = ?', year) }
  scope :by_month, ->(month) { where('extract(month from created_at) + 0 = ?', month) }
  scope :similarvin_acts, ->(act) { where("id != ? and vin = ? and vin <> '' ", act.id, act.vin) }
  scope :similarnumber_acts, ->(act) { where("id != ? and ts_number = ? and ts_number <> '' ", act.id, act.ts_number) }

  scope :with_eager_loaded_images, -> { eager_load(images_attachments: :blob) }
  scope :with_eager_loaded_additions, -> { eager_load(additions_attachments: :blob) }

  def stale?
    Time.zone.now.to_date - date.to_date > 2 && status_id == 2
  end

  def inthework?
    Time.zone.now.to_date - date.to_date <= 2 && status_id == 2
  end

  def self.last_exp
    Act.where(created_at: 15.days.ago..Time.zone.now).where('nexp is NOT NULL ').where(expert_id: Current.user).maximum(:nexp)
  end

  def self.last_act
    Act.where(created_at: 15.days.ago..Time.zone.now).where('nact is NOT NULL ').where(specialist_id: Current.user).maximum(:nact)
  end

  def image_urls
    images.map{|p| Rails.application.routes.url_helpers.url_for(p) }
  end

  def addition_urls
    additions.map{|p| Rails.application.routes.url_helpers.url_for(p) }
  end

  def self.ransackable_attributes(auth_object = nil)
    ["city_id", "collect", "complete_on", "created_at", "crime", "customer_id", "date", "datexp", "done", "dop", "expert_id", "id", "km", "loss", "loss_bill", "loss_full", "loss_wear", "nact", "nal", "ncase", "nexp", "notpresent", "owner", "owner_phone", "place", "polis_number", "polis_type_id", "prt", "pvu", "repair", "specialist_id", "start_date", "start_time", "status", "status_id", "summ_act", "summ_exp", "summ_prm", "summ_viezd", "summ_viezd_exp", "ts_mark", "ts_model", "ts_number", "updated_at", "user_id", "viezd", "vin", "workshop_id", "worktype_id"]
  end

  def self.ransackable_associations(auth_object = nil)
    ["additions_attachments", "additions_blobs", "audits", "calculations_attachments", "calculations_blobs", "city", "comments", "customer", "expert", "images_attachments", "images_blobs", "polis_type", "repair_payment_statuses", "repair_payment_timelines", "repair_statuses", "repair_timelines", "rich_text_content", "specialist", "status", "user", "workshop", "worktype"]
  end

  before_save :upcase_fields, :set_cost, :set_viezd

  # after_commit on: :update do |act|
  #   ActRelayJob.perform_later(act)
  # end

  private

  def upcase_fields
    vin.upcase!
    ts_number.upcase!
    ts_mark.upcase!
    ts_model.upcase!
  end

  def set_cost
    # self.owner = worktype.name
    if worktype_id == 1 && Price.where(user_id: customer, worktype_id: worktype).present?
      self.summ_act = Price.where(user_id: customer, worktype_id: '2').pluck(:cost).join(' ')
      self.summ_exp = Price.where(user_id: customer, worktype_id: worktype).pluck(:cost).join(' ')
    elsif worktype_id == 2 && Price.where(user_id: customer, worktype_id: worktype).present?
      self.summ_act = Price.where(user_id: customer, worktype_id: worktype).pluck(:cost).join(' ')
    end
  end

  def set_viezd
    if summ_viezd.present?
      self.viezd = true
    end
  end

  def in_work?
    status_id == 2
  end

  before_save do |complete|
    complete.complete_on = Time.zone.now if done == 1
    complete.complete_on = nil if done.nil?
    complete.complete_on = nil if done == 0
  end
end
