# frozen_string_literal: true

# == Schema Information
#
# Table name: audits
#
#  id             :bigint           not null, primary key
#  item_type      :string           not null
#  item_id        :integer          not null
#  event          :string           not null
#  whodunnit      :string
#  object         :text
#  object_changes :text
#  created_at     :datetime
#
class Audit < PaperTrail::Version
  self.table_name = :audits
  belongs_to :user, foreign_key: 'whodunnit', optional: true, inverse_of: :audits
end
