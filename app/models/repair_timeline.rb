# == Schema Information
#
# Table name: repair_timelines
#
#  id               :bigint           not null, primary key
#  act_id           :integer
#  repair_status_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class RepairTimeline < ApplicationRecord
  belongs_to :act
  belongs_to :repair_status
end
