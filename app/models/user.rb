# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :bigint           default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  name                   :string(255)
#  role                   :bigint
#  active                 :boolean          default(TRUE), not null
#  settings               :jsonb            not null
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  tokens                 :text
#  allow_password_change  :boolean          default(TRUE), not null
#
class User < ApplicationRecord
  # enum role: { admin: 0, expert: 1, specialist: 2, customer: 3, ocenshik: 4, demo: 5, viezdmaster: 6, viewer: 7 }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable
  include DeviseTokenAuth::Concerns::User
  
  store :settings, accessors: [:notify_mailer, :paging_type, :paging_per, :gallery_type]

  after_initialize :initialize_settings_defaults, :if => :new_record?

  def initialize_settings_defaults
    self.paging_per = 20
  end

  has_many :assignments, dependent: :destroy
  has_many :roles, through: :assignments

  accepts_nested_attributes_for :roles

  encrypts :private_api_key, deterministic: true
  
  has_many :acts, dependent: :nullify, strict_loading: true
  has_many :evaluations, dependent: :nullify, strict_loading: true
  has_many :prices
  has_many :worktypes, through: :prices
  has_many :audits, foreign_key: 'whodunnit', dependent: :nullify, inverse_of: :user
  has_many :workshops

  accepts_nested_attributes_for :workshops, reject_if: :all_blank #, allow_destroy: true

  validates :email, :name, presence: true
  validates :uid, uniqueness: { scope: :provider }

  scope :expert, -> { joins(:roles).where(roles: { name: 'expert' }) }
  scope :specialist, -> { joins(:roles).where(roles: { name: 'specialist' }) }
  scope :customer, -> { joins(:roles).where(roles: { name: 'customer' }) }
  scope :appraiser, -> { joins(:roles).where(roles: { name: 'appraiser' }) }
  scope :viewer, -> { joins(:roles).where(roles: { name: 'viewer' }) }
  scope :workshop, -> { joins(:roles).where(roles: { name: 'workshop' }) }
  scope :repairmaster, -> { joins(:roles).where(roles: { name: 'repairmaster' }) }

  # before_create :create_uid
  before_validation :init_uid
  before_validation :init_name

  before_create :set_private_api_key

  validates :private_api_key, uniqueness: true, allow_blank: true

  # private

  def init_uid
    self.uid = email if uid.blank? && provider == 'email'
  end

  def create_uid
    self.uid = SecureRandom.uuid
  end

  def init_name
    self.name = email if name.blank?
  end

  def role?(role)
    roles.any? { |r| r.name.underscore.to_sym == role }
  end

  def admin?
    role?(:admin)
  end

  def customer?
    role?(:customer)
  end

  def specialist?
    role?(:specialist)
  end

  def expert?
    role?(:expert)
  end

  def appraiser?
    role?(:appraiser)
  end

  def viewer?
    role?(:viewer)
  end

  def workshop?
    role?(:workshop)
  end

  def repairmaster?
    role?(:repairmaster)
  end

  def set_private_api_key
    self.private_api_key = SecureRandom.hex if self.private_api_key.nil?
  end

  def self.ransackable_attributes(auth_object = nil)
    ["active", "allow_password_change", "created_at", "current_sign_in_at", "current_sign_in_ip", "email", "encrypted_password", "id", "last_sign_in_at", "last_sign_in_ip", "name", "private_api_key", "provider", "remember_created_at", "reset_password_sent_at", "reset_password_token", "role", "settings", "sign_in_count", "tokens", "uid", "updated_at"]
  end
  
end
