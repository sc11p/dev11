# == Schema Information
#
# Table name: discussions
#
#  id         :bigint           not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Discussion < ApplicationRecord
  has_many :comments, as: :commentable
end
