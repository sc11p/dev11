# frozen_string_literal: true

# == Schema Information
#
# Table name: prices
#
#  id          :bigint           not null, primary key
#  user_id     :bigint
#  worktype_id :bigint
#  cost        :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Price < ApplicationRecord
  belongs_to :user
  belongs_to :worktype
  validates :cost, presence: true

  def self.ransackable_attributes(auth_object = nil)
    ["cost", "created_at", "id", "updated_at", "user_id", "worktype_id"]
  end
  
end
