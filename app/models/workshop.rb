# frozen_string_literal: true

# == Schema Information
#
# Table name: workshops
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  email      :string
#  tel        :string
#  address    :string
#  manager    :string
#  active     :boolean          default(TRUE), not null
#
class Workshop < ApplicationRecord
  has_many :workshoplands
  has_many :cities, through: :workshoplands
  belongs_to :user

  def self.ransackable_attributes(auth_object = nil)
    ["active", "address", "created_at", "email", "id", "manager", "name", "tel", "updated_at", "user_id"]
  end

end
