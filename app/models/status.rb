# frozen_string_literal: true

# == Schema Information
#
# Table name: statuses
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#
class Status < ApplicationRecord
  has_many :acts
  has_many :evaluations
end
