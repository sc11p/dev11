# frozen_string_literal: true

# == Schema Information
#
# Table name: evaluations
#
#  id             :bigint           not null, primary key
#  nact           :bigint
#  date           :date
#  appraiser_id   :bigint
#  oldcomments    :string(255)
#  city_id        :bigint
#  place          :string(255)
#  otype_id       :bigint
#  status_id      :bigint
#  user_id        :bigint           not null
#  created_at     :datetime
#  updated_at     :datetime
#  customer_id    :bigint
#  specialist_id  :bigint
#  summ_act       :bigint
#  summ_viezd     :bigint
#  summ_viezd_exp :bigint
#  summ_exp       :bigint
#  nocenka        :bigint
#  nal            :boolean          default(FALSE)
#  viezd          :bigint
#  km             :bigint
#  datexp         :date
#  object         :string(255)
#  ncase          :string(255)
#  polis_number   :string(255)
#  owner          :string(255)
#  done           :boolean          default(FALSE), not null
#  polis_type_id  :bigint           default(3), not null
#  collect        :boolean          default(FALSE), not null
#  paymentcontrol :boolean          default(FALSE), not null
#
class Evaluation < ApplicationRecord
  has_many :comments, as: :commentable

  has_many_attached :images
  has_many_attached :additions

  has_rich_text :content
  
  belongs_to :customer, class_name: 'User', foreign_key: 'customer_id', optional: true
  belongs_to :appraiser, class_name: 'User', foreign_key: 'appraiser_id', optional: true
  belongs_to :specialist, class_name: 'User', foreign_key: 'specialist_id', optional: true
  belongs_to :city
  belongs_to :otype
  belongs_to :status
  belongs_to :user
  belongs_to :polis_type

  validates :object, :date, :otype_id, :customer_id, :specialist_id, :city_id, :status_id, :user_id, presence: true

  scope :inthework, -> { where(status_id: '2') }
  scope :unregistered, -> { where(status_id: '1') }
  scope :done, -> { where(status_id: '3') }
  scope :paymentcontrol, -> { where(paymentcontrol: true) }
  scope :stale, -> { where('Time.zone.now.to_date - evaluation.date.to_date < ?', 2) }
  scope :by_year, ->(year) { where('extract(year from created_at) + 0 = ?', year) }
  scope :by_month, ->(month) { where('extract(month from created_at) + 0 = ?', month) }

  scope :with_eager_loaded_images, -> { eager_load(images_attachments: :blob) }
  scope :with_eager_loaded_additions, -> { eager_load(additions_attachments: :blob) }

  def stale?
    Time.zone.now.to_date - date.to_date > 2 && status_id == 2
  end

  def inthework?
    Time.zone.now.to_date - date.to_date <= 2 && status_id == 2
  end

  def done?
    status_id == 3
  end

  def image_urls
    images.map{|p| Rails.application.routes.url_helpers.url_for(p) }
  end

  def addition_urls
    additions.map{|p| Rails.application.routes.url_helpers.url_for(p) }
  end

  def self.ransackable_attributes(auth_object = nil)
    ["appraiser_id", "city_id", "collect", "created_at", "customer_id", "date", "datexp", "done", "id", "km", "nact", "nal", "ncase", "nocenka", "object", "oldcomments", "otype_id", "owner", "paymentcontrol", "place", "polis_number", "polis_type_id", "specialist_id", "status_id", "summ_act", "summ_exp", "summ_viezd", "summ_viezd_exp", "updated_at", "user_id", "viezd"]
  end

end
