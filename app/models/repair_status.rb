# == Schema Information
#
# Table name: repair_statuses
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class RepairStatus < ApplicationRecord
  has_many :repair_timelines
  has_many :acts, through: :repair_timelines

  def self.ransackable_attributes(auth_object = nil)
    ["created_at", "id", "name", "updated_at"]
  end
  
end
