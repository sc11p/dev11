# == Schema Information
#
# Table name: workshoplands
#
#  id          :bigint           not null, primary key
#  city_id     :integer
#  workshop_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Workshopland < ApplicationRecord
  belongs_to :workshop
  belongs_to :city
end
  
