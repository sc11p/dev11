class RobotsController < ApplicationController
  def show
    skip_authorization
    render "show", layout: false, content_type: "text/plain"
  end 
end
