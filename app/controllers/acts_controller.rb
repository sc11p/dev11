# frozen_string_literal: true

class ActsController < ApplicationController
  include GenerateZip
  before_action :authenticate_user!
  before_action :set_current_user
  before_action :set_act, only: %i[show edit update destroy delete_images delete_calculations delete_additions]
  before_action :set_search, except: %i[show edit update new create destroy book]

  def index
    @acts = @q.result(distinct: true)
        .page(params[:page])
        .per(current_user.paging_per || 25)
    @repair_statuses = RepairStatus.all
    @repair_payment_statuses = RepairPaymentStatus.all
    authorize @acts
    respond_to do |format|
      format.html
      format.js do
        render 'kaminari/infinite-scrolling', locals: { objects: @acts }
      end # index.html.erb
    end
  end

  def show
    authorize @act
    @similarvin_acts = Act.similarvin_acts(@act)
    @similarnumber_acts = Act.similarnumber_acts(@act)
    @comments =
      if params[:comment]
        @act.comments.where(id: params[:comment])
      else
        @act.comments.where(parent_id: nil)
      end

    @comments = @comments.page(params[:page]).per(5)
    @filename = @act.id

    respond_to do |format|
      format.html
      format.zip { send_zip @act.images }
    end
  end

  def new
    @act = Act.new
    authorize @act
  end

  def edit
    authorize @act
  end

  def create
    @act = Act.new(act_params)
    authorize @act
    @act.user_id = current_user.id

    if @act.save
      # ImagePreprocessJob.perform_later(@act)
      NotifierMailer.with(act: @act).create_email.deliver_now
      redirect_to @act, notice: 'Акт успешно создан.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    authorize @act
    if @act.update(act_params)
      redirect_to @act, notice: 'Акт успешно обновлен.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize @act
    @act.images.purge_later
    @act.additions.purge_later
    @act.destroy!
    redirect_to @act, notice: 'Акт успешно удален.'
  end

  def delete_images
    authorize @act
    @act.images.purge_later
    redirect_back(fallback_location: acts_path)
  end

  def delete_calculations
    authorize @act
    @act.calculations.purge_later
    redirect_back(fallback_location: acts_path)
  end

  def delete_additions
    authorize @act
    @act.additions.purge_later
    redirect_back(fallback_location: acts_path)
  end

  def book
    if params[:year].present? && params[:month].present?
      @acts =
        policy_scope(Act).by_month(params[:month]).by_year(
          params[:year]
        )
      authorize @acts
      fname =
        (current_user.name + '_месяц_' + params[:month] + '_год_' + params[:year])
      render xlsx: 'book', formats: :xlsx, filename: fname
    else
      skip_authorization
      redirect_back(fallback_location: acts_path)
    end
  end

  def download_xls
    @acts = @q.result
    authorize @acts
    respond_to do |format|
      format.xlsx do
        response.headers['Content-Disposition'] = 'inline; filename="act.xlsx"'
      end
    end
  end

  def clodup
    @act = Act.find(params[:act_id])
    authorize @act
    @act.summ_act = nil
    @act.summ_viezd = nil
    @act.summ_viezd_exp = nil
    @act.summ_exp = nil
    @act.summ_prm = nil
    @act.specialist_id = nil
    @act.nal = nil
    @act.created_at = DateTime.now
    @act = Act.new(@act.attributes)
    render :new
  end

  def book_generate
    authorize Act
    if params[:year].present? && params[:month].present?
      recipient = current_user.email
      BookGenerateJob.perform_later(
        params[:user],
        params[:month],
        params[:year],
        recipient
      )
    end
      redirect_back(fallback_location: acts_path)
  end

  private

  def set_act
    @act = Act.find(params[:id])
  end

  def set_search
    @q = policy_scope(Act).ransack(params[:q])
    @q.sorts = ["#{params[:sorts]} desc", 'id desc'] if @q.sorts.empty?
  end

  def set_current_user
    Current.user = current_user
  end

  def act_params
    params.require(:act).permit(
      :worktype_id,
      :nexp,
      :nact,
      :specialist_id,
      :date,
      :dop,
      :customer_id,
      :polis_type_id,
      :polis_number,
      :pvu,
      :ncase,
      :city_id,
      :place,
      :viezd,
      :km,
      :datexp,
      :loss,
      :status_id,
      :done,
      :expert_id,
      :owner,
      :owner_phone,
      :ts_mark,
      :ts_model,
      :ts_number,
      :vin,
      :summ_act,
      :summ_viezd,
      :summ_viezd_exp,
      :summ_exp,
      :summ_prm,
      :nal,
      :prt,
      :crime,
      :start_time,
      :start_date,
      :collect,
      :notpresent,
      :content,
      :workshop_id,
      :repair,
      :loss_wear,
      :loss_full,
      :loss_bill,
      :created_at,
      repair_status_ids: [],
      repair_payment_status_ids: [],
      additions: [], calculations: [], images: []
    )
  end
end
