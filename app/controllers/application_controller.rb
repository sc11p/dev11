# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit::Authorization
    
  after_action :verify_authorized, except: :index, unless: :devise_controller?
  after_action :verify_policy_scoped, only: :index, unless: :devise_controller?
  
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
 
  protect_from_forgery with: :exception 
  #skip_before_action :verify_authenticity_token, if: :json_request?
  
  before_action :set_current_user, unless: :devise_controller?
  before_action :set_paper_trail_whodunnit
  
  private

  def json_request?
    request.format.json?
  end

  def set_current_user
    @user ||= current_user
    Current.user = @user
  end

  def safe_params
    params.except(:host, :port, :protocol).permit!
  end

  def user_not_authorized
    flash[:error] = "Вы не управомочены на это действие."
    redirect_to request.headers["Referer"] || root_path
  end
  
  helper_method :safe_params
  
end
