# frozen_string_literal: true

class HomeController < ApplicationController
  protect_from_forgery with: :exception
  
  skip_after_action :verify_policy_scoped

  def index
    @summ_all_day = (Act.where('created_at > ?', 1.months.ago).sum(:summ_act) + Act.where('created_at > ?', 1.months.ago).sum(:summ_exp) + Act.where('created_at > ?', 1.months.ago).sum(:summ_viezd)) / 30
    @summ_alle_day = (Evaluation.where('created_at > ?', 1.months.ago).sum(:summ_act) + Evaluation.where('created_at > ?', 1.months.ago).sum(:summ_exp) + Evaluation.where('created_at > ?', 1.months.ago).sum(:summ_viezd)) / 30
  end
end
