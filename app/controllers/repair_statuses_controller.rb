# frozen_string_literal: true

class RepairStatusesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_repair_status, only: %i[show edit update destroy]

  # GET /repair_statuses
  # GET /repair_statuses.json
  def index
    # @repair_statuses = RepairStatus.all
    @q = policy_scope(RepairStatus).ransack(params[:q])
    @q.sorts = 'id asc' if @q.sorts.empty?
    @repair_statuses = @q.result(distinct: true)
  end

  # GET /repair_statuses/1
  # GET /repair_statuses/1.json
  def show
    authorize @repair_status
  end

  # GET /repair_statuses/new
  def new
    @repair_status = RepairStatus.new
    authorize @repair_status
  end

  # GET /repair_statuses/1/edit
  def edit
    authorize @repair_status
  end

  # POST /repair_statuses
  # POST /repair_statuses.json
  def create
    @repair_status = RepairStatus.new(repair_status_params)
    authorize @repair_status

    respond_to do |format|
      if @repair_status.save
        format.html do
          redirect_to @repair_status, notice: 'RepairStatus was successfully created.'
        end
        format.json { render :show, status: :created, location: @repair_status }
      else
        format.html { render :new }
        format.json do
          render json: @repair_status.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /repair_statuses/1
  # PATCH/PUT /repair_statuses/1.json
  def update
    authorize @repair_status
    respond_to do |format|
      if @repair_status.update(repair_status_params)
        format.html do
          redirect_to @repair_status, notice: 'RepairStatus was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @repair_status }
      else
        format.html { render :edit }
        format.json do
          render json: @repair_status.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /repair_statuses/1
  # DELETE /repair_statuses/1.json
  def destroy
    authorize @repair_status
    @repair_status.destroy
    respond_to do |format|
      format.html do
        redirect_to repair_statuses_url,
                    notice: 'RepairStatus was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_repair_status
    @repair_status = RepairStatus.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def repair_status_params
    params.require(:repair_status).permit(:name)
  end
end
