# frozen_string_literal: true

class WorkshopsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_workshop, only: %i[show edit update destroy]

  # GET /workshops
  # GET /workshops.json
  def index
    # @workshops = Workshop.all
    @q = policy_scope(Workshop).ransack(params[:q])
    @q.sorts = 'id asc' if @q.sorts.empty?
    @workshops = @q.result(distinct: true)
  end

  # GET /workshops/1
  # GET /workshops/1.json
  def show
    authorize @workshop
  end

  # GET /workshops/new
  def new
    @workshop = Workshop.new
    authorize @workshop
  end

  # GET /workshops/1/edit
  def edit
    authorize @workshop
  end

  # POST /workshops
  # POST /workshops.json
  def create
    
    @workshop = Workshop.new(workshop_params)
    authorize @workshop

    respond_to do |format|
      if @workshop.save
        format.html do
          redirect_to @workshop, notice: 'Workshop was successfully created.'
        end
        format.json { render :show, status: :created, location: @workshop }
      else
        format.html { render :new }
        format.json do
          render json: @workshop.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /workshops/1
  # PATCH/PUT /workshops/1.json
  def update
    authorize @workshop
    respond_to do |format|
      if @workshop.update(workshop_params)
        format.html do
          redirect_to @workshop, notice: 'Workshop was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @workshop }
      else
        format.html { render :edit }
        format.json do
          render json: @workshop.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /workshops/1
  # DELETE /workshops/1.json
  def destroy
    authorize @workshop
    @workshop.destroy
    respond_to do |format|
      format.html do
        redirect_to workshops_url, notice: 'Workshop was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_workshop
    @workshop = Workshop.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def workshop_params
    params.require(:workshop).permit(:user_id,
    :address,
    :tel,
    :manager,
    :email,
    :name,
    city_ids: [],
    )
  end
end
