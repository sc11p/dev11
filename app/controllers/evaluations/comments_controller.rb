class Evaluations::CommentsController < CommentsController
  before_action :set_commentable

  private

  def set_commentable
    @commentable = Evaluation.find(params[:evaluation_id])
  end
end
