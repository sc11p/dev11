# frozen_string_literal: true

class ChartsController < ApplicationController

  skip_after_action :verify_authorized

  def acts_by_month
    render json: Act.group_by_month(:created_at).count
  end

  def acts_by_year
    render json: Act.group_by_year(:created_at).count
  end

  def expert_book_by_month
    render json: Act.where('created_at > ? AND expert_id = ?', 6.months.ago, current_user).group_by_month(:created_at, format: '%-m.%Y').sum(:summ_exp)
  end

  def expert_exps_by_month
    render json: Act.where('created_at > ? AND expert_id = ?', 6.months.ago, current_user).group_by_month(:created_at, format: '%-m.%Y').count
  end

  def specialist_book_by_month
    render json: Act.where('created_at > ? AND specialist_id = ?', 6.months.ago, current_user).group_by_month(:created_at, format: '%-m.%Y').sum(:summ_act)
  end

  def specialist_acts_by_month
    render json: Act.where('created_at > ? AND specialist_id = ?', 6.months.ago, current_user).group_by_month(:created_at, format: '%-m.%Y').count
  end

  def all_acts_by_month
    render json: Act.where('created_at > ?', 6.months.ago).group_by_month(:created_at, format: '%-m.%Y').count
  end

  def all_bud_book_by_month
    render json: Act.where('created_at > ? AND customer_id = ?', 6.months.ago, '648').group_by_month(:created_at, format: '%-m.%Y').sum(:summ_exp)
  end

  def all_customer_acts_by_month
    render json: Act.where('created_at > ? AND customer_id = ?', 6.months.ago, current_user).group_by_month(:created_at, format: '%-m.%Y').count
  end

  def all_acts_per_day
    render json: Act.where('created_at > ?', 7.days.ago).group_by_day(:created_at, format: "%d %b").count
  end

  def all_evaluations_per_day
    render json: Evaluation.where('created_at > ?', 7.days.ago).group_by_day(:created_at, format: "%d %b").count
  end

  def all_worktype_per_month
    render json: Act.where('created_at > ?', 6.month.ago).group(:worktype_id).group_by_month(:created_at).count.chart_json
  end

  def all_evaluation_per_month
    render json: Evaluation.where('created_at > ?', 6.months.ago).group_by_month(:created_at, format: '%-m.%Y').sum(:summ_exp)
  end

  def all_evaluation_act_per_month
    render json: Evaluation.where('created_at > ?', 6.months.ago).group_by_month(:created_at, format: '%-m.%Y').sum(:summ_act)
  end

  def all_act_per_month
    render json: Act.where('created_at > ?', 6.months.ago).group_by_month(:created_at, format: '%-m.%Y').sum(:summ_exp)
  end

  def all_act_act_per_month
    render json: Act.where('created_at > ?', 6.months.ago).group_by_month(:created_at, format: '%-m.%Y').sum(:summ_act)
  end

end
