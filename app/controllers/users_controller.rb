class UsersController < ApplicationController

  before_action :authenticate_user!
  before_action :set_user, only: %i[show edit update destroy]

  def index
    @q = policy_scope(User).ransack(params[:q])
    @q.sorts = 'id asc' if @q.sorts.empty?
    @users = @q.result(distinct: true)
    authorize User
  end

  def show
    authorize @user
  end

  def new
    @user = User.new
  end

  def edit
    authorize @user
    session[:return_to] ||= request.referer
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, notice: 'Пользователь успешно создан.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    authorize @user
    if @user.update(user_params)
      if session[:return_to]
        redirect_to session.delete(:return_to), notice: 'Данные пользователя успешно обновлены.'
      else
        redirect_to @user, notice: 'Данные пользователя успешно обновлены.'
      end
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize @user
    @user.destroy
    redirect_to users_url, notice: 'Пользователь успешно удален.'
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(
      :name,
      :email,
      :active,
      :notify_mailer,
      :paging_type,
      :paging_per,
      :gallery_type,
      role_ids: [],
      workshop_ids: [],
      workshops_attributes: [:id, :name, :_destroy]
    )
  end
end