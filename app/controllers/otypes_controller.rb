# frozen_string_literal: true

class OtypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_otype, only: %i[show edit update destroy]

  # GET /otypes
  # GET /otypes.json
  def index
    # @otypes = Otype.all
    @q = policy_scope(Otype).ransack(params[:q])
    @q.sorts = 'id asc' if @q.sorts.empty?
    @otypes = @q.result(distinct: true)
  end

  # GET /otypes/1
  # GET /otypes/1.json
  def show
    authorize @otype
  end

  # GET /otypes/new
  def new
    @otype = Otype.new
    authorize @otype
  end

  # GET /otypes/1/edit
  def edit
    authorize @otype
  end

  # POST /otypes
  # POST /otypes.json
  def create
    @otype = Otype.new(otype_params)
    authorize @otype

    respond_to do |format|
      if @otype.save
        format.html do
          redirect_to @otype, notice: 'Otype was successfully created.'
        end
        format.json { render :show, status: :created, location: @otype }
      else
        format.html { render :new }
        format.json do
          render json: @otype.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /otypes/1
  # PATCH/PUT /otypes/1.json
  def update
    authorize @otype
    respond_to do |format|
      if @otype.update(otype_params)
        format.html do
          redirect_to @otype, notice: 'Otype was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @otype }
      else
        format.html { render :edit }
        format.json do
          render json: @otype.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /otypes/1
  # DELETE /otypes/1.json
  def destroy
    authorize @otype
    @otype.destroy
    respond_to do |format|
      format.html do
        redirect_to otypes_url, notice: 'Otype was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_otype
    @otype = Otype.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def otype_params
    params.require(:otype).permit(:name)
  end
end
