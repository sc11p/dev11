module Api
  module V1
    class EvaluationsController < Api::V1::ApiController
      
      skip_before_action :check_json_request
      before_action :set_evaluation, only: %i[show update destroy]
      before_action :set_search, except: %i[show edit update new create destroy]

      def index
        # @evaluations = policy_scope(evaluation).page(params[:page]).per(25)
        @evaluations = @q.result
                .includes([:otype])
                .includes([:specialist])
                .includes([:appraiser])
                .includes([:customer])
                .includes([:status])
                .includes([:city])
                .page(params[:page])
                .per(current_user.paging_per || 25)

        authorize @evaluations
        
        render json: EvaluationPreviewSerializer.new(@evaluations, meta: pagination_dict(@evaluations)).serializable_hash
      end

      def create
        @evaluation = Evaluation.new(evaluation_params)
        authorize @evaluation
        if @evaluation.save
          # render json: @evaluation, status: 200
          render json: EvaluationSerializer.new(@evaluation).serializable_hash
        else
          render json: @evaluation.errors, status: :unprocessable_entity
        end
      end

      def show
        #render json: @evaluation, status: 200
        authorize @evaluation
        render json: EvaluationSerializer.new(@evaluation).serializable_hash
      end

      def update
        authorize @evaluation
        render json: EvaluationSerializer.new(@evaluation).serializable_hash, status: :ok if @evaluation.update!(evaluation_params)
      end

      private

      def evaluation_params
        params.require(:evaluation).permit(
          :nact,
          :nocenka,
          :date,
          :datexp,
          :viezd,
          :appraiser_id,
          :customer_id,
          :specialist_id,
          :user_id,
          :city_id,
          :place,
          :otype_id,
          :status_id,
          :summ_act,
          :summ_viezd,
          :summ_viezd_exp,
          :summ_exp,
          :nal,
          :object,
          :ncase,
          :polis_number,
          :owner,
          :km,
          :done,
          :polis_type_id,
          :content,
          :oldcomments,
          :collect,
          :paymentcontrol,
          additions: [], images: []
        )
      end

      def set_evaluation
        @evaluation = Evaluation.find(params[:id])
      end

      def set_search
        @q = policy_scope(Evaluation).ransack(params[:q])
        @q.sorts = ["#{params[:sorts]} desc", 'id desc'] if @q.sorts.empty?
      end

    end
  end
end
