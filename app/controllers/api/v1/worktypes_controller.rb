module Api
  module V1
    class WorktypesController < Api::V1::ApiController
      before_action :auth_user

      def index
        @worktypes = Worktype.all
        skip_policy_scope
        render json: WorktypeSerializer.new(@worktypes).serializable_hash
      end
            
      private

      def auth_user
        authorize current_user
      end
 
    end
  end
end