module Api
  module V1
    class StatusesController < Api::V1::ApiController
      before_action :auth_user

      def index
        @statuses = Status.all
        skip_policy_scope
        render json: StatusSerializer.new(@statuses).serializable_hash
      end
            
      private

      def auth_user
        authorize current_user
      end
 
    end
  end
end