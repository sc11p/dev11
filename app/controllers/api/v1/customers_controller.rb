module Api
  module V1
    class CustomersController < Api::V1::ApiController
      before_action :auth_user

      def index
        @customers = User.customer
        skip_policy_scope
        render json: CustomerSerializer.new(@customers).serializable_hash
      end
            
      private

      def auth_user
        authorize current_user
      end
 
    end
  end
end