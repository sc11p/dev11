module Api
    module V1
      class PolisTypesController < Api::V1::ApiController
        before_action :auth_user
  
        def index
          @polis_types = PolisType.all
          skip_policy_scope
          render json: PolisTypeSerializer.new(@polis_types).serializable_hash
        end
              
        private
  
        def auth_user
          authorize current_user
        end
   
      end
    end
  end