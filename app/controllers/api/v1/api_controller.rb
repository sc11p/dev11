module Api
  module V1
    class ApiController < ApplicationController
      
      include Api::Concerns::ActAsApiRequest
      include DeviseTokenAuth::Concerns::SetUserByToken

      #include Pundit::Authorization
      
      before_action :authenticate_user!, except: :status
      skip_after_action :verify_authorized, only: :status

      layout false
      respond_to :json

      rescue_from Pundit::NotAuthorizedError,          with: :user_not_authorized
      rescue_from ActiveRecord::RecordNotFound,        with: :render_not_found
      rescue_from ActiveRecord::RecordInvalid,         with: :render_record_invalid
      rescue_from ActionController::ParameterMissing,  with: :render_parameter_missing

      def status
        render json: { online: true }
      end

      private

      def pagination_dict(collection)
        {
          current_page: collection.current_page,
          next_page: collection.next_page,
          prev_page: collection.prev_page,
          total_pages: collection.total_pages,
          total_count: collection.total_count
        }
      end

      def user_not_authorized(exception)
        logger.info { exception }
        render json: { error: I18n.t('api.errors.unauthorized') }, status: :unauthorized
      end

      def render_not_found(exception)
        logger.info { exception } # for logging
        render json: { error: I18n.t('api.errors.not_found') }, status: :not_found
      end

      def render_record_invalid(exception)
        logger.info { exception } # for logging
        render json: { errors: exception.record.errors.as_json }, status: :bad_request
      end

      def render_parameter_missing(exception)
        logger.info { exception } # for logging
        render json: { error: I18n.t('api.errors.missing_param') }, status: :unprocessable_entity
      end
      
      def set_user_by_token(mapping = nil)
        api_key = request.headers[:api_access_token] || request.headers[:HTTP_API_ACCESS_TOKEN]
        
        if api_key.nil?
          super mapping
        else
          user = User.find_by_private_api_key (api_key)
            sign_in(:user, user)
            rc = resource_class(mapping)
            scope = rc.to_s.underscore.to_sym
            bypass_sign_in(user, scope: scope)
            return user
        end
      end
      
      def pundit_user
        set_user_by_token
      end

      def resource_name
        :user
      end
    
      def resource
        @resource ||= User.new
      end
    
      def devise_mapping
        @devise_mapping ||= Devise.mappings[:user]
      end
    end
  end
end
