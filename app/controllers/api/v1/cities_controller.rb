module Api
  module V1
    class CitiesController < Api::V1::ApiController
      before_action :auth_user

      def index
        @cities = City.all
        skip_policy_scope
        render json: CitySerializer.new(@cities).serializable_hash
      end
            
      private

      def auth_user
        authorize current_user
      end
 
    end
  end
end