module Api
  module V1
    class ActsController < Api::V1::ApiController
      # before_action :auth_user
      skip_before_action :check_json_request
      before_action :set_act, only: %i[show update destroy]
      before_action :set_search, except: %i[show edit update new create destroy]

      def index
        @acts = @q.result
                .includes([:status])
                .includes([:worktype])
                .includes([:polis_type])
                .includes([:specialist])
                .includes([:expert])
                .includes([:customer])
                .includes([:status])
                .includes([:city])
                .includes([:workshop])
                .page(params[:page])
                .per(current_user.paging_per || 25)

        authorize @acts
        render json: ActPreviewSerializer.new(@acts, meta: pagination_dict(@acts)).serializable_hash
      end

      def new
        @act = Act.new
        authorize @act
      end

      def create
        @act = Act.new(act_params)
        @act.user_id = current_user.id
        authorize @act
        if @act.save
          render json: ActSerializer.new(@act).serializable_hash, status: 200
        else
          render json: @act.errors, status: :unprocessable_entity
        end
      end

      def update
        authorize @act
        render json: ActSerializer.new(@act).serializable_hash, status: :ok if @act.update!(act_params)
      end

      def show
        authorize @act
        options = {
          include: [:repair_timelines, :repair_payment_timelines]
        }
        render json: ActSerializer.new(@act, options).serializable_hash, status: 200
      end

      private

      def set_act
        @act = Act.find(params[:id])
      end

      def set_search
        @q = policy_scope(Act).ransack(params[:q])
        @q.sorts = ["#{params[:sorts]} desc", 'id desc'] if @q.sorts.empty?
      end

      # def auth_user
      #   authorize current_user
      # end

      def act_params
        params.require(:act).permit(
          :worktype_id,
          :nexp,
          :nact,
          :specialist_id,
          :date,
          :dop,
          :customer_id,
          :polis_type_id,
          :polis_number,
          :pvu,
          :ncase,
          :city_id,
          :place,
          :viezd,
          :km,
          :datexp,
          :loss,
          :status_id,
          :done,
          :expert_id,
          :owner,
          :owner_phone,
          :ts_mark,
          :ts_model,
          :ts_number,
          :vin,
          :summ_act,
          :summ_viezd,
          :summ_viezd_exp,
          :summ_exp,
          :summ_prm,
          :nal,
          :prt,
          :crime,
          :start_time,
          :collect,
          :notpresent,
          :content,
          :repair,
          additions: [], images: []
        )
      end

    end
  end
end
