class Acts::CommentsController < CommentsController
  before_action :set_commentable

  private

  def set_commentable
    @commentable = Act.find(params[:act_id])
  end
end
