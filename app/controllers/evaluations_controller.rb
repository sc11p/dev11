# frozen_string_literal: true

class EvaluationsController < ApplicationController
  include GenerateZip
  before_action :authenticate_user!
  before_action :set_current_user
  before_action :set_evaluation, only: %i[show edit update destroy delete_images delete_additions]
  before_action :set_search, except: %i[show edit update new create destroy]

  def index
    @evaluations =
      @q.result(distinct: true)
        .page(params[:page])
        .per(current_user.paging_per || 25)
    
    authorize Evaluation

    respond_to do |format|
      format.html
      format.js do
        render 'kaminari/infinite-scrolling', locals: { objects: @evaluations }
      end # index.html.erb
    end
  end

  def show
    authorize @evaluation
    @comments =
      if params[:comment]
        @evaluation.comments.where(id: params[:comment])
      else
        @evaluation.comments.where(parent_id: nil)
      end

    @comments = @comments.page(params[:page]).per(5)
    @filename = @evaluation.id

    respond_to do |format|
      format.html
      format.zip { send_zip @evaluation.images }
    end
  end

  def new
    @evaluation = Evaluation.new
    authorize @evaluation
  end

  def edit
    authorize @evaluation
  end

  def create
    @evaluation = Evaluation.new(evaluation_params)
    authorize @evaluation
    @evaluation.user_id = current_user.id

    if @evaluation.save
      redirect_to @evaluation, notice: 'Оценка успешно добавлена.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    authorize @evaluation
    if @evaluation.update(evaluation_params)
      redirect_to @evaluation, notice: 'Оценка успешно обновлена.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize @evaluation
    @evaluation.images.purge_later
    @evaluation.additions.purge_later
    @evaluation.destroy!
    redirect_to evaluations_url, notice: 'Оценка успешно удалена.'
  end

  def book
    if params[:year].present? && params[:month].present?
      @evaluations =
        policy_scope(Evaluation).by_month(params[:month])
          .by_year(params[:year])
      authorize @evaluations
      fname =
        (current_user.name + '_месяц_' + params[:month] + '_год_' + params[:year])
      render xlsx: 'book',formats: :xlsx, filename: fname
    else
      skip_authorization
      redirect_back(fallback_location: evaluations_path)
    end
  end

  def delete_images
    authorize @evaluation
    @evaluation.images.purge_later
    redirect_back(fallback_location: evaluations_path)
  end

  def delete_additions
    authorize @evaluation
    @evaluation.additions.purge_later
    redirect_back(fallback_location: evaluations_path)
  end

  def download_xls
    @evaluations = @q.result
    authorize @evaluations
    respond_to do |format|
      format.xlsx do
        response.headers['Content-Disposition'] =
          'inline; filename="evaluation.xlsx"'
      end
    end
  end

  def clodup
    @evaluation = Evaluation.find(params[:evaluation_id])
    authorize @evaluation
    @evaluation = Evaluation.new(@evaluation.attributes)
    render :new
  end

  private

  def set_evaluation
    @evaluation = Evaluation.find(params[:id])
  end

  def set_search
    @q = policy_scope(Evaluation).ransack(params[:q])
    @q.sorts = ["#{params[:sorts]} desc", 'id desc'] if @q.sorts.empty?
  end

  def set_current_user
    Current.user = current_user
  end

  def evaluation_params
    params.require(:evaluation).permit(
      :nact,
      :nocenka,
      :date,
      :datexp,
      :viezd,
      :appraiser_id,
      :customer_id,
      :specialist_id,
      :user_id,
      :city_id,
      :place,
      :otype_id,
      :status_id,
      :summ_act,
      :summ_viezd,
      :summ_viezd_exp,
      :summ_exp,
      :nal,
      :object,
      :ncase,
      :polis_number,
      :owner,
      :km,
      :done,
      :polis_type_id,
      :content,
      :oldcomments,
      :collect,
      :paymentcontrol,
      additions: [], images: []
    )
  end
end
