# frozen_string_literal: true

class PricesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_price, only: %i[show edit update destroy]

  # GET /prices
  # GET /prices.json
  def index
    # @prices = Price.all
    @q = policy_scope(Price).ransack(params[:q])
    @q.sorts = 'id asc' if @q.sorts.empty?
    @prices = @q.result(distinct: true)
  end

  # GET /prices/1
  # GET /prices/1.json
  def show
    authorize @price
  end

  # GET /prices/new
  def new
    @price = Price.new
    authorize @price
  end

  # GET /prices/1/edit
  def edit
    authorize @price
  end

  # POST /prices
  # POST /prices.json
  def create
    
    @price = Price.new(price_params)
    authorize @price

    respond_to do |format|
      if @price.save
        format.html do
          redirect_to @price, notice: 'Price was successfully created.'
        end
        format.json { render :show, status: :created, location: @price }
      else
        format.html { render :new }
        format.json do
          render json: @price.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /prices/1
  # PATCH/PUT /prices/1.json
  def update
    authorize @price
    respond_to do |format|
      if @price.update(price_params)
        format.html do
          redirect_to @price, notice: 'Price was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @price }
      else
        format.html { render :edit }
        format.json do
          render json: @price.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /prices/1
  # DELETE /prices/1.json
  def destroy
    authorize @price
    @price.destroy
    respond_to do |format|
      format.html do
        redirect_to prices_url, notice: 'Price was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_price
    @price = Price.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def price_params
    params.require(:price).permit(:worktype_id, :user_id, :cost)
  end
end
