# frozen_string_literal: true

class AuditsController < ApplicationController
  
  skip_after_action :verify_policy_scoped

  
  def index
    @audits = act.audits.reorder(id: :desc)
  end

  private

  helper_method def act
    @act ||= Act.find(params[:act_id])
  end
end
