# frozen_string_literal: true

class WorktypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_worktype, only: %i[show edit update destroy]

  # GET /worktypes
  # GET /worktypes.json
  def index
    # @worktypes = Worktype.all
    @q = policy_scope(Worktype).ransack(params[:q])
    @q.sorts = 'id asc' if @q.sorts.empty?
    @worktypes = @q.result(distinct: true)
  end

  # GET /worktypes/1
  # GET /worktypes/1.json
  def show
    authorize @worktype
  end

  # GET /worktypes/new
  def new
    @worktype = Worktype.new
    authorize @worktype
  end

  # GET /worktypes/1/edit
  def edit
    authorize @worktype
  end

  # POST /worktypes
  # POST /worktypes.json
  def create
    @worktype = Worktype.new(worktype_params)
    authorize @worktype

    respond_to do |format|
      if @worktype.save
        format.html do
          redirect_to @worktype, notice: 'Worktype was successfully created.'
        end
        format.json { render :show, status: :created, location: @worktype }
      else
        format.html { render :new }
        format.json do
          render json: @worktype.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /worktypes/1
  # PATCH/PUT /worktypes/1.json
  def update
    authorize @worktype
    respond_to do |format|
      if @worktype.update(worktype_params)
        format.html do
          redirect_to @worktype, notice: 'Worktype was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @worktype }
      else
        format.html { render :edit }
        format.json do
          render json: @worktype.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /worktypes/1
  # DELETE /worktypes/1.json
  def destroy
    authorize @worktype
    @worktype.destroy
    respond_to do |format|
      format.html do
        redirect_to worktypes_url,
                    notice: 'Worktype was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_worktype
    @worktype = Worktype.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def worktype_params
    params.require(:worktype).permit(:name)
  end
end
