import consumer from './consumer'

document.addEventListener('turbo:load', () => {
  const act = document.querySelector('act')
  if (!act) return

  const actId = act.dataset.cableActId
  if (!actId) return

  consumer.subscriptions.create({
    channel: 'ActUpdateChannel', act_id: actId
  }, {
    connected() {
      // Called when the subscription is ready for use on the server
      console.log('Connected.')
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
      console.log('Disconnected.')
    },

    received() {
      // Called when there's incoming data on the websocket for this channel
      console.log('Received data.')

      // Reload the page
      Turbolinks.visit(window.location)
    }
  })
})