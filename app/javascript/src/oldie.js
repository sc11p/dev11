
import 'viewerjs'
import 'justifiedGallery'
import justifiedGallery from 'justifiedGallery'

document.addEventListener('turbo:load', () => {

  $('.gallery').justifiedGallery({
      rowHeight: 100,
      selector: 'div',
      cssAnimation: false,
      margins: 1,
    })

    .on('jg.complete', function () {
      const viewer = new Viewer(document.getElementById('images'), {
        url: 'data-original',
        title: false,
        transition: false,
        toolbar: {
          zoomIn: 4,
          zoomOut: 4,
          oneToOne: 4,
          reset: 4,
          prev: 4,
          play: false,
          next: 4,
          rotateLeft: 4,
          rotateRight: 4,
          flipHorizontal: 4,
          flipVertical: 4,
        },
      })
    })
})

