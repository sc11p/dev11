import Masonry from "masonry-layout"
import "masonry-layout"
import imagesLoaded from "imagesloaded"
import Viewer from 'viewerjs'

document.addEventListener('turbo:load', () => {

  var grid = document.querySelector('.imageViewer');
  var msnry = new Masonry(grid, {
    itemSelector: '.imageViewer-item',
    //columnWidth: '.imageViewer-sizer',
    columnWidth: 100,
    percentPosition: true,
    horizontalOrder: true
  });

  msnry.once( 'layoutComplete', function() {
    console.log('layout done, just this one time');
    const viewer = new Viewer(document.getElementById('images'), {
      url: 'data-original',
      title: false,
      transition: false,
      toolbar: {
        zoomIn: 4,
        zoomOut: 4,
        oneToOne: 4,
        reset: 4,
        prev: 4,
        play: false,
        next: 4,
        rotateLeft: 4,
        rotateRight: 4,
        flipHorizontal: 4,
        flipVertical: 4,
      },
    })
  });

  imagesLoaded(grid).on('progress', function () {
    // layout Masonry after each image loads
    msnry.layout();
  });

})