import '@hotwired/turbo-rails'
import * as ActiveStorage from '@rails/activestorage'

import './bootstrap_custom.js'

import "chartkick/chart.js"

import "trix"
import "@rails/actiontext"

import flatpickr from "flatpickr";
import { Russian } from 'flatpickr/dist/l10n/ru.js'

import "./src/jquery"
import "./src/jquery-ui"

//import './channels'
//import "./src/oldie"
import "./src/imageViewer"
import './controllers'

document.addEventListener('turbo:load', () => {
  flatpickr(".datepicker", {
    locale: 'ru',
    allowInput: true,
    //dateFormat: 'YYYY-MM-DD'
  });

  flatpickr(".datetimepicker", {
    locale: 'ru',
    format: 'YYYY-MM-DD HH:mm',
    enableTime: true,
    time_24hr: true,
    minTime: '09:00',
    maxTime: '18:30',
    stepping: 30,
  });

  flatpickr(".timepicker", {
    dateFormat: 'H:i',
    noCalendar: true,
    enableTime: true,
    time_24hr: true,
    minTime: '09:00',
    maxTime: '18:30',
    allowInput: true,
    minuteIncrement: 30,
  });
});


ActiveStorage.start()
