class ImagePreprocessJob < ApplicationJob
  queue_as :default
  
  def perform(act)
    act.images.each do |image|
      image.variant(resize_to_limit: [100, 100]).processed
      image.variant(resize_to_limit: [2048, 2048]).processed
    end
  end
  
end
