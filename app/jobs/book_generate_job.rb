# frozen_string_literal: true

class BookGenerateJob < ApplicationJob
  require 'rubygems'
  require 'zip'
  queue_as :default

  def perform(user, month, year, recipient)

    acts = Act.where("specialist_id = ? OR expert_id = ?", user, user).by_month(month).by_year(year)
    evaluations = Evaluation.where("specialist_id = ? OR appraiser_id = ?", user, user).by_month(month).by_year(year)
    @file_name = ('БД_Отчет ' + month + '.' + year + ' ' + User.where("id = ?", user).map(&:name).join(''))
    user_name = User.where("id = ?", user).map(&:name).join('')
    user_id = User.where("id = ?", user).map(&:id).join('').to_i
    
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: "Акты") do |sheet|
        sheet.add_row %w[
                   ФИОСотрудника
                   №Сотрудника
                   Город
                   Датаакт
                   Датаэксп
                   Номеракт
                   Номерэксп
                   Заказчик
                   Убыток
                   ТипПолис
                   ФИОКлиент
                   ТС
                   ТСномер
                   ТСвин
                   Полис
                   Услуга
                   Сборданет
                   Сбор
                   Выезд
                   КМ
                   СтоимостьАкта
                   СтоимостьКалькуляции
                   СтоимостьВыезда
                   Нал
                   ДСоздания
                   ДОбновления
                   ]

        acts.each do |act|
          viddogovora = if act.polis_type_id == 1
                          'ОСАГО'
                        elsif act.polis_type_id == 2
                          'КАСКО'
                        else
                          'НЕТ'
                        end
          viezd_vsego = (act.summ_viezd.to_i + act.summ_viezd_exp.to_i).to_s 
          sbor = if act.collect == true
                      'да'
                    else
                      ''
                    end
            
          sheet.add_row [
            user_name,
            user_id,
            act.city.name,
            #if act.worktype_id == 2 or act.worktype_id == 5 or act.worktype_id == 12
            #  act.date
            #else
            #  act.datexp
            #end,
            act.date,
            act.datexp,
            if act.nact.present?
              act.nact
            else
            end,
            act.nexp,
            act.customer.name,
            act.ncase,
            viddogovora,
            act.owner,
            act.ts_mark + ' ' + act.ts_model,
            act.ts_number,
            act.vin,
            act.polis_number,
            act.worktype.name,
            sbor,
            if act.collect == true
              act.summ_prm
            else
            end,
            if act.viezd == true && act.specialist_id == user_id 
              'Выезд'
            else
            end,
            if act.viezd == true && user_id == act.specialist_id
              act.km
            else
            end,
            if user_id == act.specialist_id
              act.summ_act
            else
            end,
            if act.expert.present? && user_id == act.expert_id
              act.summ_exp
            else
            end,
            if act.viezd == true && user_id == act.specialist_id
              viezd_vsego
            else
            end,
            if act.nal == true
              'Нал.'
            else
              'Б/нал.'
            end,
            act.created_at,
            act.updated_at
          ]
        end
      end
      p.workbook.add_worksheet(name: "Оценка") do |sheet|
        sheet.add_row %w[
          ФИОСотрудника
          №Сотрудника
          Город
          Датаакт
          Датаэксп
          Номеракт
          Номерэксп
          Заказчик
          Убыток
          ТипПолис
          ФИОКлиент
          ТС
          ТСномер
          ТСвин
          Полис
          Услуга
          Сборданет
          Сбор
          Выезд
          КМ
          СтоимостьАкта
          СтоимостьКалькуляции
          СтоимостьВыезда
          Нал
          ДСоздания
          ДОбновления
          ]

        evaluations.each do |evaluation|
        viddogovora = if evaluation.polis_type_id == 1
                        'ОСАГО'
                      elsif evaluation.polis_type_id == 2
                        'КАСКО'
                      else
                        'НЕТ'
                      end
        viezd_vsego = (evaluation.summ_viezd.to_i + evaluation.summ_viezd_exp.to_i).to_s

        sbor = if evaluation.collect == true
          'да'
        else
          ''
        end
        
        sheet.add_row [
          user_name,
          user_id,
          evaluation.city.name,
          evaluation.date,
          evaluation.datexp,
          if evaluation.nact.present?
            evaluation.nact
          else
          end,
          evaluation.nocenka,
          evaluation.customer.name,
          evaluation.ncase,
          viddogovora,
          evaluation.owner,
          evaluation.place,
          evaluation.object,
          '',
          evaluation.polis_number,
          evaluation.otype.name,
          sbor,
          '',
          if evaluation.viezd == 1 && evaluation.specialist_id == user_id 
            'Выезд'
          else
          end,
          if evaluation.viezd == 1 && user_id == evaluation.specialist_id
            evaluation.km
          else
          end,
          if user_id == evaluation.specialist_id
            evaluation.summ_act
          else
          end,
          if evaluation.appraiser.present? && user_id == evaluation.appraiser_id
            evaluation.summ_exp
          else
          end,
          if evaluation.viezd == 1 && user_id == evaluation.specialist_id
            viezd_vsego
          else
          end,
          if evaluation.nal == true
            'Нал.'
          else
            'Б/нал.'
          end,
          evaluation.created_at,
          evaluation.updated_at
        ]
        end
      end
      p.serialize("#{Rails.root}/tmp/#{@file_name}.xlsx")
    end

    zipfile_name = "#{Rails.root}/tmp/book.zip"
    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
      zipfile.add("#{@file_name}.xlsx", "#{Rails.root}/tmp/#{@file_name}.xlsx")
    end

    #NotifierMailer.with(file_name: @file_name).send_book.deliver_now
    NotifierMailer.with(file_name: 'book.zip', topic_name: @file_name, recipient: recipient).send_book.deliver_now
    FileUtils.rm("#{Rails.root}/tmp/#{@file_name}.xlsx")
    FileUtils.rm(zipfile_name)
  end
end
