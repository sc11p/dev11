# frozen_string_literal: true

class ActRelayJob < ApplicationJob
  queue_as :default

  def perform(act)
    ActUpdateChannel.broadcast_to act, ActSerializer.new.serialize(act)
  end
  end
