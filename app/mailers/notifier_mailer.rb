class NotifierMailer < ApplicationMailer
  default from: 'database@rocenka.ru'
    
  def create_email
    @act = params[:act]

    if @act.customer_id == 602 && @act.specialist_id == 19 || @act.customer_id == 602 && @act.specialist_id == 32 || @act.customer_id == 602 && @act.specialist_id == 56
      recipient = 'mychko@rocenka.ru'
      # @url = 'http://example.com/login'
      # @customer = @act.customer
      mail(to: recipient, subject: 'Сформировано новое дело')
    end
  end

  def send_book
    @file_name = params[:file_name]
    @topic_name = params[:topic_name]

    @recipient = params[:recipient]
    attachments["#{@file_name}"] = File.read("#{Rails.root}/tmp/#{@file_name}")
    mail(to: @recipient, subject: "Сформирована книга регистрации #{@topic_name}")
  end
  
end
