class UserPolicy < ApplicationPolicy
  def index?
    true
  end
  
  def show?
    update?
  end

  def edit?
    update?
  end

  def update?
    record.id == user.id || @user.admin?
  end

  def destroy?
    user && @user.admin?
  end
  
end