class ActPolicy < ApplicationPolicy
  class Scope
    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if user.admin? || user.viewer?
        scope.all
      #elsif user.workshop?
        #scope.joins(:workshop).all
      elsif user.repairmaster?
        #scope.where(workshop: true)
        scope.where(repair: true).or(scope.where(workshop: true))
      #scope.joins(:workshop).where(workshop: [user.workshops])
      else
        scope.includes(:workshop).where('user_id = ? OR customer_id =? OR specialist_id = ? OR expert_id =? OR workshop_id IN (?)', user.id, user.id, user.id, user.id, user.workshops.map(&:id) )
      end
    end

    # private

    attr_reader :user, :scope
  end

  def index?
    user
  end

  def create?
    user
  end

  def show?
    user && @user.admin? || user && @user.viewer? || user && @user.repairmaster? || user && @user.workshop? || user && (record.user_id == user.id || record.specialist_id == user.id || record.expert_id == user.id || record.customer_id == user.id  ) 
  end

  def edit?
    update?
  end
  
  def update?
    #true
    user && @user.admin? || user && @user.repairmaster? || user && (record.user_id == user.id || record.specialist_id == user.id || record.expert_id == user.id || user.workshops.exists?(record.workshop_id)) && (not record.done?)
  end
 
  def new?
    create?
  end

  def clodup?
    user
  end
  
  def destroy?
    user && @user.admin?
  end

  def delete_images?
    user && @user.admin?
  end

  def delete_calculations?
    user && @user.admin?
  end

  def delete_additions?
    user && @user.admin?
  end

  def book_generate?
    user
  end

  def book?
    user
  end

  def download_xls?
    user
  end

end