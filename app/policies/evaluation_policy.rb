class EvaluationPolicy < ApplicationPolicy
  class Scope
    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if user.admin? || user.viewer?
        scope.all
      else
        scope.where('user_id = ? OR customer_id =? OR specialist_id = ? OR appraiser_id =?', user.id, user.id, user.id, user.id)
      end
    end

    private

    attr_reader :user, :scope
  end

  def index?
    true
  end

  def show?
    @user.admin? || (record.user_id == user.id || record.specialist_id == user.id || record.appraiser_id == user.id || record.customer_id == user.id ) 
  end

  def edit?
    update?
  end
  
  def update?
    @user.admin? || (record.user_id == user.id || record.specialist_id == user.id || record.appraiser_id == user.id )
  end

  def create?
    true
  end

  def new?
    create?
  end

  def clodup?
    true
  end
  
  def destroy?
    @user.admin?
  end

  def delete_images?
    @user.admin?
  end

  def delete_additions?
    user && @user.admin?
  end

  def book?
    true
  end

  def download_xls?
    true
  end

end