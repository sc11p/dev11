class OtypePolicy < ApplicationPolicy
    class Scope
      def initialize(user, scope)
        @user = user
        @scope = scope
      end
  
      def resolve
        if user.admin?
          scope.all
        else
          scope.all
        end
      end
  
      private
  
      attr_reader :user, :scope
    end
  
    def index?
      true
    end

    def show?
      @user.admin? 
    end

    def edit?
      update?
    end

    def update?
      @user.admin? 
    end
  
    def create?
      true
    end
  
    def new?
      create?
    end
          
    def destroy?
      @user.admin?
    end
  
  end