json.id         user.id
json.email      user.email
json.name       user.name
json.role       user.role
json.active     user.active
json.created_at user.created_at
