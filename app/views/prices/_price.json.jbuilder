# frozen_string_literal: true

json.extract! price, :id, :worktype_id, :cost, :created_at, :updated_at
json.url price_url(price, format: :json)
