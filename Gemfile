# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 7.0.7'
# Use postgresql as the database for Active Record
gem 'pg', '~> 1.5'

gem 'puma', '~> 6.3'

#gem 'sprockets-rails'
gem "cssbundling-rails"

gem 'propshaft'

gem 'jsbundling-rails'

gem 'stimulus-rails'

gem 'turbo-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# gem 'panko_serializer'
# gem 'active_model_serializers'
gem 'jsonapi-serializer'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'aws-sdk-s3', require: false

gem 'pundit'
# gem 'cancancan' # , '~> 1.9', require: false
gem 'chartkick'
gem 'devise'
gem "devise_token_auth", git: "https://github.com/lynndylanhurley/devise_token_auth.git", branch: "master"
gem 'rack-cors', require: 'rack/cors'
gem 'jbuilder'
# gem 'oj'
gem 'groupdate'
gem 'haml'
gem 'haml-rails', '~> 2.1'
gem 'kaminari'
gem 'ransack'
gem 'caxlsx_rails'
gem 'caxlsx'
gem 'rubyzip'
gem 'simple_form'
# gem 'calculate-all'
# gem 'gon'
# gem 'elastic_email_rails'
gem 'mailgun-ruby'
gem 'image_processing'
gem 'mini_magick'
gem 'paper_trail'
gem 'redis'
# gem 'searchkick'
gem 'sidekiq'
gem 'rollbar'
gem 'inline_svg', '~> 1.9'
# gem 'skylight'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

gem 'net-smtp', require: false

group :development, :test do
  gem 'bullet'
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem 'pry-byebug', platform: :mri
  gem 'pry-rails'
  gem 'rspec_api_documentation'
  gem 'rspec-rails'
end

group :development do
  gem 'annotate'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'brakeman'
  gem 'i18n-tasks'
  gem 'letter_opener'
  gem 'listen'
  gem 'rails_best_practices'
  gem 'reek'
  gem 'rubocop-rails'
  gem 'spring'
  #gem 'spring-watcher-listen'
end

group :test do
  gem 'faker'
  gem 'shoulda-matchers'
  gem 'pundit-matchers'
  gem 'simplecov', require: false
  gem 'webmock'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
