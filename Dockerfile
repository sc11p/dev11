######################
# Stage: Builder
FROM ruby:3.2.2-alpine as Builder

ARG FOLDERS_TO_REMOVE
ARG BUNDLE_WITHOUT
ARG RAILS_ENV
# ARG RAILS_MASTER_KEY

ENV BUNDLE_WITHOUT ${BUNDLE_WITHOUT}
ENV RAILS_ENV ${RAILS_ENV}
ENV SECRET_KEY_BASE=foo
ENV RAILS_SERVE_STATIC_FILES=true
# ENV RAILS_MASTER_KEY $RAILS_MASTER_KEY
ENV LANG=C.UTF-8

# Add basic packages
RUN apk add --no-cache \
      build-base \
      gcompat \
      vips-dev \
      postgresql-dev \
      git \
      nodejs \
      npm \
      yarn \
      tzdata \
      file

WORKDIR /app

# Install standard gems
COPY Gemfile* /app/
RUN bundle config --local frozen 1 && \
    bundle config --local build.sassc --disable-march-tune-native && \
    bundle install -j4 --retry 3

#### ONBUILD: Add triggers to the image, executed later while building a child image

# Install Ruby gems (for production only)
ONBUILD COPY Gemfile* /app/
ONBUILD RUN bundle config --local without 'development test' && \
            bundle install -j4 --retry 3 && \
            # Remove unneeded gems
            bundle clean --force && \
            # Remove unneeded files from installed gems (cached *.gem, *.o, *.c)
            rm -rf /usr/local/bundle/cache && \
            find /usr/local/bundle/gems/ -name "*.c" -delete && \
            find /usr/local/bundle/gems/ -name "*.o" -delete

# Install yarn packages
COPY package.json yarn.lock .yarnclean /app/
RUN yarn install

# Add the Rails app
ADD . /app

# Precompile assets
# RUN if [[ "$RAILS_ENV" == "production" ]];     then rails assets:precompile;  fi
RUN if [[ "$RAILS_ENV" == "production" ]];     then rails assets:precompile;  fi

# Remove folders not needed in resulting image
RUN rm -rf $FOLDERS_TO_REMOVE

###############################
# Stage Final
FROM ruby:3.2.2-alpine
LABEL maintainer="evtmail@gmail.com"

ARG ADDITIONAL_PACKAGES

# Add Alpine packages
RUN apk add --update --no-cache \
    postgresql-client \
    imagemagick \
    ffmpeg \
    mupdf \
    mupdf-tools \
    vips \
    $ADDITIONAL_PACKAGES \
    tzdata \
    file

# Add user
RUN addgroup -g 1000 -S app \
 && adduser -u 1000 -S app -G app
USER app

# Copy app with gems from former build stage
COPY --from=Builder --chown=app:app /usr/local/bundle/ /usr/local/bundle/
COPY --from=Builder --chown=app:app /app /app

# Set Rails env
ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true

WORKDIR /app

# Expose Puma port
EXPOSE 3000

# Save timestamp of image building
RUN date -u > BUILD_TIME

# Enable YJIT
ENV RUBY_YJIT_ENABLE=1

# Start up
CMD ["docker/startup.sh"]
