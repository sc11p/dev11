require 'sidekiq/web'

Rails.application.routes.draw do

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      #get :status, to: 'api#status'
      mount_devise_token_auth_for 'User', at: 'users', controllers: {
        registrations: 'api/v1/registrations',
        sessions: 'api/v1/sessions',
        passwords: 'api/v1/passwords'
      }
      devise_scope :user do
        resource :user, only: %i[update show]
      end
      resources :acts
      resources :evaluations
      resources :cities
      resources :worktypes
      resources :statuses
      resources :polis_types
      resources :customers
    end
  end
  devise_for :users
  root 'home#index'
  get "/robots.txt", to: "robots#show"
  mount Sidekiq::Web => '/sidekiq'

  resources :prices, :ratings, :users, :worktypes, :otypes, :workshops, :repair_statuses
  
  namespace :user do
    resource :private_api_keys, only: :update
  end

  resources :actions do
  end

  match '/acts/operation', to: 'acts#book', via: 'get'
  match '/acts/bg', to: 'acts#book_generate', via: 'get'
  match '/evaluations/operation', to: 'evaluations#book', via: 'get'

  resources :charts, only: [] do
    collection do
      get 'acts_by_month'
      get 'acts_by_year'
      get 'specialist_acts_by_month'
      get 'specialist_book_by_month'
      get 'expert_exps_by_month'
      get 'expert_book_by_month'
      get 'all_acts_by_month'
      get 'all_bud_book_by_month'
      get 'all_customer_acts_by_month'
      get 'all_acts_per_day'
      get 'all_evaluations_per_day'
      get 'all_worktype_per_month'
      get 'all_evaluation_per_month'
      get 'all_evaluation_act_per_month'
      get 'all_act_per_month'
      get 'all_act_act_per_month'
    end
  end

  resources :acts do
    resources :audits, only: [ :index ]
    resources :comments, module: :acts
    delete :delete_images, on: :member
    delete :delete_calculations, on: :member
    delete :delete_additions, on: :member
    get 'clodup'
    collection do
      get 'download_xls', defaults: { format: 'xlsx' }
      get 'counts'
    end
  end

  resources :evaluations do
    resources :comments, module: :evaluations
    delete :delete_images, on: :member
    delete :delete_additions, on: :member
    get 'clodup'
    collection do
      get 'download_xls', defaults: { format: 'xlsx' }
    end
  end

  resources :discussions do
    resources :comments, module: :discussions
  end
end
